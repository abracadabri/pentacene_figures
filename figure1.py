from os.path import join, curdir
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
from matplotlib.collections import PatchCollection
import matplotlib.patches as patches
from mpl_toolkits.axisartist.axislines import SubplotZero
from matplotlib.patches import Circle, Rectangle
import matplotlib.image as mpimg
from style import dark_red, light_red, dark_blue, light_blue
import seaborn as sns
import scipy.io as spio
import crystals as crystals
from scipy import ndimage
from matplotlib import rcParams
from utils import add_label
import matplotlib.ticker as plticker
    
def gaussian(x, mu, sig):
    return np.exp(-np.power(x - mu, 2.) / (2 * np.power(sig, 2.)))

plt.close()
plt.style.use(join(curdir, "style", "nature_com_presentation.mplstyle"))

colors = sns.color_palette("GnBu", 3) + sns.color_palette("Reds", 9)


def fig_1b(ax, datapath="data"):
    
    peaks_subset = np.loadtxt(datapath + '/peaks_to_label.txt')

    d2 = np.load(datapath + '/nice_pattern.npy')
    #d2[d2 == np.nan] = 0
    pcm = ax.pcolormesh(np.log(d2),  vmin = -5, vmax = -1.5, cmap = 'cividis')
    #pcm = ax.pcolormesh(np.log(d2),  vmin = -5, vmax = -1.5, cmap = 'Greys')
    #cbaxes = fig.add_axes([0.4, 0.55, 0.2, 0.02])
    #cbar = plt.colorbar(pcm, cax=cbaxes)
    #print(np.min(np.log(d2)))
    #print(np.max(np.log(d2)))
    shiftx = 0
    shifty = 0
    
    labels = ['($020$)','($0\overline{2}0$)', '($\overline{1}10$)', '($1\overline{1}0$)', '($2\overline{3}0$)','($450$)', '($\overline{53}0$)']
    for idx, p in enumerate(peaks_subset):
        c = Circle((p[0] + shiftx, p[1] + shifty), 20, edgecolor = 'w', facecolor = 'None')
        c = Circle((p[0] + shiftx, p[1] + shifty), 20, edgecolor = 'b', facecolor = 'None')
        ax.add_patch(c)
        #ax.plot(p[0], p[1] , marker = 'x', color = 'w')
        #print(p[0], p[1])
        ax.text(p[0], p[1] , labels[idx],  color = 'w')
        ax.text(p[0], p[1] , labels[idx],  color = 'b')
    
    
    pcm.set_rasterized(True)
    ax.axis('square')
    ax.set_xticks([])
    ax.set_yticks([])
    
def fig_1c(ax, datapath="data"):
    
    dat_abs = np.loadtxt(datapath + '/190809_Absorption/VIS/60nm_1.txt')
    dat_ref = np.loadtxt(datapath + '/190809_Absorption/VIS/background.txt')
    offset = 0.22
    wl = dat_abs[:,0]
    I = dat_abs[:,1] - offset
    
    wlref = dat_ref[:,0]
    Iref = dat_ref[:,1]
    

    idx_680 = np.argmin(np.abs(wl - 670))
    print(I[idx_680])
    
    gauss = I[idx_680]*gaussian(wl, 680, 10) 
    
    ax.plot(wl, I, color = sns.xkcd_rgb["pinkish red"])
    ax.fill_between(wl, 0, gauss, color = sns.xkcd_rgb["pinkish red"], alpha = 0.2)
    ax.plot(wl, gauss,color = sns.xkcd_rgb["pinkish red"])
    ax.text(668, 0.02, "Laser")
    ax.text(660, 0.12, 'B$_1$')
    ax.text(624, 0.10, 'A$_1$')
    ax.text(570, 0.10, 'B$_2$/A$_2$')
    ax.text(535, 0.07, 'B$_3$')
    #ax.plot(wlref, Iref)
    ax.set_xlim(500, 700)
    ax.set_ylim(0.01, 0.14)
    ax.set_yticks([])
    ax.set_xlabel('Wavelength [nm]')
    ax.set_ylabel('Intensity [a.u.]')
    loc = plticker.MultipleLocator(base= 50) # this locator puts ticks at regular intervals
    ax.xaxis.set_minor_locator(loc)
    
def figure_1():
    
    w = (4/3)*3.37
    l = 2.2
    fig = plt.figure(constrained_layout=False, figsize=(w, l))
    
    
    gs0 = fig.add_gridspec(
            1,
            2,
            width_ratios=[1, 0.9],
            bottom=0.05,
            top=0.98,
            left=0.0,
            right=0.98,
            hspace=0.0,
            wspace = 0.2
        )
        
    ax_a1 = fig.add_subplot(gs0[0,0])
    ax_a2 = fig.add_subplot(gs0[0, 1])

    #add_label(ax_a1, '(b)', color = 'white')
    fig_1b(ax_a1)
    #add_label(ax_a2, '(c)')
    fig_1c(ax_a2)
    
    
    lratio = 0.1
    rratio = 0.95
    tratio = 0.95
    bratio = 0.05
    
    plt.subplots_adjust(left=lratio, right=rratio, top=tratio, bottom=bratio)
    
    plt.savefig("figure1_partial.png", dpi = 600, transparent=True)
    plt.show()


def figure_1b():
    
    w = 2*3.37
    l = 3.37
    fig = plt.figure(constrained_layout=False, figsize=(w, l))
    
    
    gs0 = fig.add_gridspec(
            2,
            2,
            width_ratios=[0.7, 1],
            height_ratios = [1, 0.6],
            bottom=0.05,
            top=0.98,
            left=0.0,
            right=0.98,
            hspace=0.0,
            wspace = 0.0
        )
        
    ax_a1 = fig.add_subplot(gs0[0:2,1])
    ax_a2 = fig.add_subplot(gs0[1, 0])

    #add_label(ax_a1, '(b)', color = 'white')
    fig_1b(ax_a1)
    #add_label(ax_a2, '(c)')
    fig_1c(ax_a2)
    
    
    lratio = 0.1
    rratio = 0.98
    tratio = 0.98
    bratio = 0.05
    
    plt.subplots_adjust(left=lratio, right=rratio, top=tratio, bottom=bratio)


figure_1()
#w = 5
#l = 3.4

#fig = plt.figure(constrained_layout=False, figsize=(w, l))
#ax = plt.gca()       
#fig_1c(ax)
#lratio = 0.1
#rratio = 0.95
#tratio = 0.98
#bratio = 0.17
    
#plt.subplots_adjust(left=lratio, right=rratio, top=tratio, bottom=bratio)
#plt.savefig('linabs_presentation_laser.png', dpi = 600, transparent = True)