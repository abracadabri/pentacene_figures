def add_label(ax, label, cx=0.1, cy=0.9, fs=10, color = 'k'):
    #{a numeric value in range 0-1000, 'ultralight', 'light', 'normal', 'regular', 'book', 'medium', 'roman', 'semibold', 'demibold', 'demi', 'bold', 'heavy', 'extra bold', 'black'}
    x0, x1, y0, y1 = ax.axis()
    ax.text(
        x0 + cx * (x1 - x0),
        y0 + cy * (y1 - y0),
        label,
        fontsize=fs,
        va='center',
        ha='center',
        fontweight='light',
        color = color
    )


def abstract_axis(ax, hw=0.05, hl=0.07, yhw=None, yhl=None):
    """
    desined to work with normalized axes
    :param ax: handle of an ax
    :param hw:
    :param hl:
    :param yhw:
    :param yhl:
    :return:
    """
    ax.axhline(linewidth=2.5, color="k")
    ax.axvline(linewidth=1.7, color="k")

    xmin, xmax = ax.get_xlim()
    ymin, ymax = ax.get_ylim()

    print(xmin, xmax)
    print(ymin, ymax)

    for direction in ["left", "bottom"]:
        ax.spines[direction].set_position('zero')
        ax.spines[direction].set_smart_bounds(True)
        ax.spines[direction].set_visible(False)

    for direction in ["right", "top"]:
        ax.spines[direction].set_color('none')
        ax.spines[direction].set_visible(False)

    lw = 1.5  # axis line width
    ohg = 0  # 0.3 # arrow overhang
    #
    # compute matching arrowhead length and width
    if yhw is None:
        yhw = hw  # /(ymax-ymin)*(xmax-xmin)* height/width
    if yhl is None:
        yhl = hl  # /(xmax-xmin)*(ymax-ymin)* width/height

    # draw x and y axis
    ax.arrow(xmin, 0, xmax - xmin, 0., fc='k', ec='k', lw=lw,
             head_width=hw, head_length=hl, overhang=ohg,
             length_includes_head=True, clip_on=False)

    ax.arrow(0, ymin, 0., ymax - ymin, fc='k', ec='k', lw=lw,
             head_width=yhw, head_length=yhl, overhang=ohg,
             length_includes_head=True, clip_on=False)
