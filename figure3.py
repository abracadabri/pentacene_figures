# -*- coding: utf-8 -*-
"""
Created on Thu Aug 27 15:04:45 2020

@author: HEL
"""

# -*- coding: utf-8 -*-
"""
Created on Thu May 28 18:51:38 2020

@author: HEL
"""

from os.path import join, curdir
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
from matplotlib.collections import PatchCollection
import matplotlib.patches as patches
from mpl_toolkits.axisartist.axislines import SubplotZero
from matplotlib.patches import Circle, Rectangle
import matplotlib.image as mpimg
from style import dark_red, light_red, dark_blue, light_blue
import seaborn as sns
import scipy.io as spio
import crystals as crystals
from scipy import ndimage
from matplotlib import rcParams
from utils import add_label
import matplotlib.ticker as plticker
from mpl_toolkits.axes_grid1.inset_locator import inset_axes
from PIL import Image


plt.close()
plt.style.use(join(curdir, "style", "nature_com.mplstyle"))

blues = sns.color_palette("GnBu_d", 6)
reds =  sns.color_palette("Reds", 9)
purples = sns.cubehelix_palette(8)
colors_peaks = [blues[0], blues[3], 'k', 'grey', 'orange', reds[4], reds[8],'green', 'grey']
labels = ['(020)','(0$\overline{2}$0)', '(1$\overline{1}$0)', '(110)', '(230)','(450)', '(530)']



    
def fig_2a(ax, datapath="data"):
    a_vdos = np.load(datapath + '/a_vdos_partial.npz')

    ax.plot(a_vdos['f'], a_vdos['xcom'], color = reds[6], label = 'X$_{\mathrm{COM}}$', linewidth = 2)
    ax.plot(a_vdos['f'], a_vdos['ycom'], color = 'orange',label = 'Y$_{\mathrm{COM}}$', linewidth = 2)
    ax.plot(a_vdos['f'], a_vdos['zcom'],color = 'b',label = 'Z$_{\mathrm{COM}}$', linewidth = 2)
    
    ax.plot(a_vdos['f'], a_vdos['x'],  color = reds[6], linestyle = 'dashed')
    ax.plot(a_vdos['f'], a_vdos['y'], color = 'orange', linestyle = 'dashed')
    ax.plot(a_vdos['f'], a_vdos['z'],  color = 'b', linestyle = 'dashed')
    #ax.plot(a_vdos['f'], a_vdos['pulse'], linestyle = 'dashed', color = 'k', linewidth = 0.5)
    ax.set_xlim(0,10)
    #ax.set_ylim(0,6.5)
    ax.set_ylim(0,10)
    ax.set_yticks([])
    ax.set_xlabel('Frequency [THz]')
    ax.set_ylabel('vDOS [arb.u.]')
    ax.xaxis.set_label_coords(0.5, -0.15)
    ax.yaxis.set_label_coords(-0.15, 0.5)
    

def fig_2a_bis(ax, datapath="data"):
    freq = np.loadtxt(datapath + '/vDOS_freq.txt')
    vdos = np.loadtxt(datapath + '/vDOS_v.txt')
    ax.plot((1/4.1)*freq, vdos, color = 'k', linewidth = 1.5)

    ax.set_xlim(0,10)
    ax.set_ylim(0,0.004)
    ax.set_xlabel('Frequency [THz]')
    ax.set_ylabel('vDOS [arb .u.]')
    ax.set_yticks([])
    
    #r = Rectangle((0, 0.), 10, 0.06, color = 'red', alpha = 0.4)
    #ax.add_patch(r)
    #ax.xaxis.set_label_coords(0.5, -0.15)
    #ax.yaxis.set_label_coords(-0.15, 0.5)
    #ax.legend()
    
def fig_2b(ax, datapath="data"):
    peaks_subset = np.loadtxt(datapath + '/peaks_to_label.txt')

    e_2D_vdos = np.load(datapath + '/e_2d_z_vdos.npz')
    e_2d_z_vdos_inset = np.load(datapath + '/e_2d_z_vdos_inlet.npz')
    pcm = ax.pcolormesh(e_2D_vdos['fi'], e_2D_vdos['fi'], np.flipud(e_2D_vdos['vdos']), cmap  = 'RdBu_r', vmin = -0.5, vmax = 0.5, rasterized = True)
    ax.axis('square')
    ax.set_xlim(0,10)
    ax.set_ylim(0,10)
    ax.set_xlabel('Frequency [THz]')
    ax.xaxis.set_label_coords(0.5, -0.15)
    ax.yaxis.set_label_coords(-0.15, 0.5)
    ax.set_ylabel('Frequency [THz]')
    ax.plot([0,10], [1,1], color = 'grey', linestyle = 'dashed', linewidth = 1)
    loc = plticker.MultipleLocator(base=2) # this locator puts ticks at regular intervals
    ax.xaxis.set_major_locator(loc)
    ax.annotate("", xy=(2, 1), xytext=(4, 4),
             arrowprops=dict(arrowstyle="->"))
    
    axin1 = ax.inset_axes([0.30, 0.4, 0.6, 0.25])
    axin1.plot([0,10],[0,0], color = 'k', linestyle = 'dashed', linewidth = 0.5)
    axin1.plot(e_2d_z_vdos_inset['x'], e_2d_z_vdos_inset['y'], color = 'grey')
    axin1.set_ylim(-0.6, 0.8)
    axin1.set_xlim(0,10)
    axin1.set_yticks([])
    axin1.tick_params(direction='in', length=3, width=1, colors='k',
               grid_color='k', grid_alpha=0.5)
    axin1.set_xlabel('Frequency [THz]', fontsize = 8)
    axin1.set_ylabel('vDOS', fontsize = 8)
    axin1.xaxis.set_label_coords(0.5, -0.6)
    axin1.yaxis.set_label_coords(-0.1, 0.5)
    
    

def fig_2c(ax, datapath="data"):
    d_vdos = np.load(datapath + '/d_vdos_partial.npz')

    ax.plot(d_vdos['f'], d_vdos['xr'], color = reds[6], label = 'X$_{\mathrm{rot}}$')
    ax.plot(d_vdos['f'], d_vdos['yr'], color = 'orange', label = 'Y$_{\mathrm{rot}}$')
    ax.plot(d_vdos['f'], d_vdos['bend'],color = reds[3], label = 'bend')
    

    ax.set_xlim(0,10)
    ax.set_ylim(0,8)
    ax.set_xlabel('Frequency [THz]')
    ax.set_ylabel('vDOS [arb .u.]')

    ax.legend()

def fig_2d(ax, datapath="data"):
    dat = np.loadtxt(datapath + '/20200303_meas3_transients_from_fitted_Helene.txt')
    time = dat[:,0]
    a = dat[:,1:]
    #error = dat[:,2::2] 
    
    idx1 = 9
    ax.plot(time, a[:,idx1] , label  = '020', color = colors_peaks[0],  marker = 'o', markerfacecolor = 'None', zorder = 0, linewidth = 2)
    #ax.fill_between(time,a[:,9]  - error[:,9], a[:,9]  + error[:,9], alpha = 0.2, color = colors[1])
    
    idx2 = 8
    ax.plot(time, a[:,idx2] , label  = '230', color = colors_peaks[1], marker = 'd', markerfacecolor = 'None', zorder = 0, linewidth = 2)
    #ax.fill_between(time,a[:,11]  - error[:,11], a[:,11]  + error[:,11], alpha = 0.2, color = colors[2])
    
    idx3 = 6
    ax.plot(time, a[:,idx3] , label  = '350', color = colors_peaks[2], marker = 's', markerfacecolor = 'None', zorder = 0, linewidth = 2, alpha = 0.5)
    #ax.fill_between(time,a[:,16]  - error[:,16], a[:,16]  + error[:,16], alpha = 0.2, color = colors[5])
    
    idx4 = 7
    ax.plot(time, a[:,idx4] , label  = '350', color = colors_peaks[3], marker = 'p', markerfacecolor = 'None', zorder = 0, linewidth = 2, alpha = 0.5)
    #ax.fill_between(time,a[:,idx4]  - error[:,idx4], a[:,idx4]  + error[:,idx4], alpha = 0.2, color = colors[8])
    
    ax.plot([-10, 300], [1, 1], linestyle = 'dashed', linewidth = 0.5, color = 'grey')
    ax.set_ylim(0.96, 1.02)
    ax.set_xlim(-1.5, 12)
    ax.yaxis.set_label_coords(-0.12,0.5)
    ax.xaxis.set_label_coords(0.5,-0.13)
    
    loc = plticker.MultipleLocator(base=1) # this locator puts ticks at regular intervals
    ax.xaxis.set_minor_locator(loc)
    loc = plticker.MultipleLocator(base=0.01) # this locator puts ticks at regular intervals
    ax.yaxis.set_minor_locator(loc)
    
    ax.set_xlabel('Time [ps]')
    ax.set_ylabel('Relative Intensity [a.u.]')
    
    axins = inset_axes(ax, width="100%", height="100%",
                    bbox_to_anchor=(.70, .37, .25, .5),
                    bbox_transform=ax.transAxes)
    fig_2e(axins)
    r = Rectangle((7, 0.98), 3, 0.03, color = 'white', alpha = 0.8)
    ax.add_patch(r)
    
    
def fig_2e(ax, datapath="data"):
    blues = sns.color_palette("GnBu_d", 6)
    colors_peaks = [blues[0], blues[3], 'k', 'grey']
    
    peak020hs = np.loadtxt(datapath + '/0-20-spline-norm-noneq.dat')
    peak020eq = np.loadtxt(datapath + '/0-20-spline-norm-eq.dat')
    ax.plot(peak020hs[:,0], peak020hs[:,1], color = blues[3])
    ax.plot(peak020eq[:,0], peak020eq[:,1], color = blues[3], linestyle = 'dashed')

    ax.set_xlim(peak020eq[0,0], 1.95)
    ax.set_ylim(0, 1.1)
    loc = plticker.MultipleLocator(base=0.5) # this locator puts ticks at regular intervals
    ax.xaxis.set_major_locator(loc)
    ax.set_xlabel('Frequency [THz]')
    ax.set_ylabel('FFT [arb.u.]')
    ax.yaxis.set_label_coords(-0.05, 0.)
    ax.set_yticks([])
    ax.set_xticklabels([])
    ax.legend(fontsize = 8, loc = 1)
    
def fig_2e2(ax, datapath="data"):
    blues = sns.color_palette("GnBu_d", 6)
    colors_peaks = [blues[0], blues[3], 'k', 'grey']
    peak020 = np.load(datapath + '/020.npz')
    peak110 = np.load(datapath + '/110.npz')
    peak110hs = np.loadtxt(datapath + '/1-10-spline-norm-noneq.dat')
    peak110eq = np.loadtxt(datapath + '/1-10-spline-norm-eq.dat')
    ax.plot(peak110hs[:,0], peak110hs[:,1], color = 'grey')
    ax.plot(peak110eq[:,0], peak110eq[:,1], color = 'grey', linestyle = 'dashed')

    
    ax.set_xlim(peak110hs[0,0], 1.95)
    ax.set_ylim(0, 1.1)
    ax.set_xlabel('Frequency [THz]')
    ax.xaxis.set_label_coords(0.5, -0.25)
    loc = plticker.MultipleLocator(base=0.5) # this locator puts ticks at regular intervals
    ax.xaxis.set_major_locator(loc)
    #ax.set_ylabel('vDOS [a.u.]')
    ax.set_yticks([])
    ax.legend()



def fig_2f(ax, datapath="data"):
    blues = sns.color_palette("GnBu_d", 6)
    colors_peaks = [blues[0], blues[3], 'k', 'grey']
    peak020_time = np.load(datapath + '/020_vs_time.npz')
    peak110_time = np.load(datapath + '/110_vs_time.npz')

    ax.plot(peak020_time['t'], peak020_time['eq'],marker = 'd',
            color = blues[3], markersize = 2, markerfacecolor = 'None')
    #ax.plot(peak110_time['t'], peak110_time['hs'])
    ax.plot(peak110_time['t'], peak110_time['eq'], marker = 'p',
            color = 'grey', markersize = 2, markerfacecolor = 'None')
    ax.set_xlim(0,15)
    ax.set_ylim(0, 1.5)
    ax.set_yticks([])
    ax.xaxis.set_label_coords(0.5, -0.4)
    ax.set_xlabel('Time [ps]')
    ax.set_ylabel('Int.[arb.u.]')
    ax.legend()
    

    

def fig_2g(ax, datapath="data"):
    saed300 = np.load(datapath + '/saed_300.npz')
    ax.axis('square')
    pat = saed300['saed']
    pat = np.flipud(np.abs(pat))
    lim = 0.1*np.max(np.abs(pat))
    im = Image.fromarray(np.int16(pat))
    im = im.rotate(20)
    ax.pcolormesh(pat, vmin =6, vmax = 10000, cmap = 'cividis', rasterized = True)
    xc = 100
    yc = 100
    ext = 100
    ax.set_xlim(xc -ext, xc + ext)
    ax.set_ylim(yc - ext, yc + ext)

    ax.set_xticks([])
    ax.set_yticks([])


def fig_2h(ax, datapath="data"):
    TDDFT = np.load(datapath +'/TDDFT_a_spin.npz') 

    pcm = ax.pcolormesh(TDDFT['time'],TDDFT['freq'],np.transpose(TDDFT['factors']/np.max(TDDFT['factors'])), cmap = 'Reds', rasterized = True)

    ax.set_ylim(0.6, 20)
    ax.set_xlabel('Time [fs]')
    loc = plticker.MultipleLocator(base=4) # this locator puts ticks at regular intervals
    ax.yaxis.set_major_locator(loc)
    loc = plticker.MultipleLocator(base=2) # this locator puts ticks at regular intervals
    ax.yaxis.set_minor_locator(loc)
    ax.xaxis.set_label_coords(0.5, -0.15)
    ax.yaxis.set_label_coords(-0.15, 0.5)
    ax.set_ylabel('Frequency [THz]')
    
    return pcm
    
    print(np.max(TDDFT['time']))
    
def fig_2i(ax, datapath="data"):
    TDDFT = np.load(datapath +'/TDDFT_a_spin.npz') 

    ax.plot(TDDFT['laser_time'],TDDFT['laser'], color = 'k')

    ax.set_xlim(0,np.max(TDDFT['laser_time']))
    ax.set_xticks([])
    ax.set_ylim(-0.12, 0.12)
    #ax.set_xlabel('Time [fs]')
    ax.set_ylabel('E [eV/${\AA}]$')
    ax.yaxis.set_label_coords(-0.15, 0.5)
    print(np.max(TDDFT['laser_time']))
    
    
def fig_2j(ax, datapath="data"):

    TDDFT_com = np.load(datapath + '/TDDFT_com.npz')
    ax.plot(TDDFT_com['time'],TDDFT_com['x1'], color = reds[6])
    ax.plot(TDDFT_com['time'],TDDFT_com['y1'], color = 'orange')
    ax.plot(TDDFT_com['time'],TDDFT_com['z1'], color = 'b')
    ax.plot(TDDFT_com['time'],TDDFT_com['x2'], color = reds[6], linestyle = 'dashed')
    ax.plot(TDDFT_com['time'],TDDFT_com['y2'], color = 'orange', linestyle = 'dashed')
    ax.plot(TDDFT_com['time'],TDDFT_com['z2'], color = 'b', linestyle = 'dashed')
    ax.set_xlim(0, 35)
    ax.set_xlabel('Time [fs]')
    ax.set_ylabel('CM movement [${\AA}$]')
    ax.xaxis.set_label_coords(0.5, -0.12)
    ax.yaxis.set_label_coords(-0.16, 0.5)
    
    

    
    
    
def figure_3():
    
    w = 1.5*3.37
    l = 4
    fig = plt.figure(constrained_layout=False, figsize=(w, l))
    
    
    gs0 = fig.add_gridspec(
            2,
            1,
            height_ratios=[1, 1],
            bottom=0.1,
            top=0.98,
            left=0.10,
            right=0.98,
            hspace=0.27
        )
        
    gs01 = gs0[0].subgridspec(1, 2, wspace=0.18, hspace = 0.1,  width_ratios=[1.5,1])
    gs02 = gs0[1].subgridspec(1, 2, wspace=0.18, width_ratios=[1.5,1])
    
    
    ax_a1 = fig.add_subplot(gs01[0,0])
    ax_a2 = fig.add_subplot(gs01[0, 1])
    ax_a3 = fig.add_subplot(gs02[0,0])
    ax_a4 = fig.add_subplot(gs02[0,1])


    fig_2a(ax_a1)
    fig_2c(ax_a3)
    pcm = fig_2b(ax_a2)
    
    lratio = 0.05
    rratio = 0.95
    tratio = 0.95
    bratio = 0.17
    
    plt.subplots_adjust(left=lratio, right=rratio, top=tratio, bottom=bratio)
    
    #cbaxes = fig.add_axes([0.06, 0.545, 0.2, 0.02])
    #cbar = plt.colorbar(pcm, cax=cbaxes, orientation="horizontal")
    #cbar.ax.set_xticklabels(['neg', '0', 'pos']) 
    
    plt.savefig("figure3_bis_test.pdf", dpi = 200, transparent=True)


def figure_3_simulated_diff():
    
    w = 1.*3.37
    l = 2.5
    fig = plt.figure(constrained_layout=False, figsize=(w, l))
    
    
    gs0 = fig.add_gridspec(
            1,
            2,
            width_ratios=[0.8, 1],
            bottom=0.15,
            top=0.98,
            left=0.10,
            right=0.98,
            hspace=0.27,
            wspace = .1
        )
        
    gs01 = gs0[0].subgridspec(2, 1, wspace=0.18, hspace = 0.2,  height_ratios=[1,1.6])
    gs02 = gs0[1].subgridspec(2, 1, wspace=0.18, height_ratios=[1,1])
    
    
    ax_a1 = fig.add_subplot(gs01[0,0])
    ax_a2 = fig.add_subplot(gs01[1,0])
    ax_a3 = fig.add_subplot(gs02[0,0])
    ax_a4 = fig.add_subplot(gs02[1,0])

    
    fig_2f(ax_a1)
    fig_2e(ax_a3)
    pcm = fig_2g(ax_a2)
    fig_2e2(ax_a4)

    lratio = 0.05
    rratio = 0.95
    tratio = 0.95
    bratio = 0.17
    
    plt.subplots_adjust(left=lratio, right=rratio, top=tratio, bottom=bratio)
    
    
    plt.savefig("figure3_diff_sim_large_pattern.svg", dpi = 200, transparent=True)



def figure_3_TDDFT():
    w = 0.8*3.37
    l = 2.5
    fig = plt.figure(constrained_layout=False, figsize=(w, l))
    
    
    gs0 = fig.add_gridspec(
            2,
            1,
            height_ratios=[1, 2.5],
            bottom=0.15,
            top=0.98,
            left=0.18,
            right=0.85,
            hspace=0.,
            wspace = .1
        )
    ax_a1 = fig.add_subplot(gs0[0])
    ax_a2 = fig.add_subplot(gs0[1])   
    fig_2i(ax_a1)
    pcm = fig_2h(ax_a2)
    
    cbaxes = fig.add_axes([0.89, 0.2, 0.02, 0.45])
    cbar = plt.colorbar(pcm, cax=cbaxes)
    

    lratio = 0.05
    rratio = 0.85
    tratio = 0.95
    bratio = 0.2
    
    
    plt.subplots_adjust(left=lratio, right=rratio, top=tratio, bottom=bratio)
    plt.savefig("figure3_TDDFT_spin.svg", dpi = 200, transparent=False)
    
def fig_2k(ax, f, corr, label):
    f = f[0:438]
    corr = corr[0:438]
    idx_neg = np.where(corr < 0.02)
    idx_pos = np.where(corr > 0.0)
    corr_neg = corr[idx_neg]
    f_neg = f[idx_neg]
    corr_pos = corr[idx_pos]
    f_pos = f[idx_pos]
    ax.plot(f_neg,corr_neg, color = blues[2])
    ax.fill_between(f_neg,corr_neg, 0, color = blues[2], alpha = 0.3)
    ax.plot(f_pos,corr_pos, color = reds[6], label = label)
    ax.fill_between(f_pos,corr_pos, 0, color = reds[6], alpha = 0.3)
    ax.plot([f[0], f[-1]], [0,0], linestyle = 'dashed', color = 'k', linewidth = 0.5)
    ax.set_ylim(-0.8, 0.8)
    ax.set_xlim(0, 15)
    loc = plticker.MultipleLocator(base=4) # this locator puts ticks at regular intervals
    ax.xaxis.set_major_locator(loc)
    loc = plticker.MultipleLocator(base=2) # this locator puts ticks at regular intervals
    ax.xaxis.set_minor_locator(loc)
    #ax.legend()
    #ax.set_xlabel('Time [fs]')
    ax.set_ylabel(label, fontsize = 6)
    #ax.xaxis.set_label_coords(0.5, -0.12)
    ax.yaxis.set_label_coords(-0.25, 0.5)

    
def figure_3_2DDOS_cuts():
    w = 0.65*3.37
    l = 2
    fig = plt.figure(constrained_layout=False, figsize=(w, l))
    
    
    gs0 = fig.add_gridspec(
            3,
            1,
            height_ratios=[1, 1, 1],
            bottom=0.15,
            top=0.98,
            left=0.25,
            right=0.98,
            hspace=0.,
            wspace = .0
        )
    ax_a1 = fig.add_subplot(gs0[0])
    ax_a2 = fig.add_subplot(gs0[1])

    ax_a3 = fig.add_subplot(gs0[2])
    #ax_a4 = fig.add_subplot(gs0[3])

    
    vDOS_cuts = np.load('data/2dvDOS.npz')
    f = vDOS_cuts['f']
    #fig_2k(ax_a1, f, vDOS_cuts['Y4ThzZ'], label = '$\phi_{\mathrm{YZ}}(4, \omega_Z)$')

    ax_a2.set_xticks([])
    fig_2k(ax_a1, f, vDOS_cuts['Z075ThzX'], label = '$\phi_{\mathrm{ZX}}(0.75, \omega_X)$')
    ax_a1.set_xticks([])
    fig_2k(ax_a2, f, vDOS_cuts['Z075ThzY'], label = '$\phi_{\mathrm{ZY}}(0.75, \omega_Y)$')
    ax_a2.set_xticks([])
    fig_2k(ax_a3, f, vDOS_cuts['Z075ThzZ'], label = '$\phi_{\mathrm{ZZ}}(0.75, \omega_Z)$')
    
    ax_a3.set_xlabel('Frequency [THz]')
    #ax_a1.yaxis.set_label_coords(-0.15, -0.0)
    ax_a3.xaxis.set_label_coords(0.5, -0.25)
    lratio = 0.2
    rratio = 0.98
    tratio = 0.95
    bratio = 0.05
    
    
    plt.subplots_adjust(left=lratio, right=rratio, top=tratio, bottom=bratio)
    plt.savefig("figure3_2DDOS_cuts.svg", dpi = 200, transparent=False)


w = 0.8*3.37
l = 2.65
fig = plt.figure(constrained_layout=False, figsize=(w, l))
ax = plt.gca()       
fig_2j(ax)
#fig_2a(ax)
#fig_2b(ax)
lratio = 0.19
rratio = 0.95
tratio = 0.93
bratio = 0.15
plt.subplots_adjust(left=lratio, right=rratio, top=tratio, bottom=bratio)
#plt.savefig('TDDFT.svg', dpi = 300, transparent = True)
#figure_3_simulated_diff()
#figure_3_TDDFT()
#figure_3()
figure_3_2DDOS_cuts()