# This import registers the 3D projection, but is otherwise unused.
from mpl_toolkits.mplot3d import Axes3D  # noqa: F401 unused import
from mpl_toolkits.mplot3d import proj3d
import numpy as np
import matplotlib.pyplot as plt


from matplotlib.patches import FancyArrowPatch

#Source :
#https://stackoverflow.com/questions/22867620/... the rest is on next line
#...putting-arrowheads-on-vectors-in-matplotlibs-3d-plot (remove ...)
class Arrow3D(FancyArrowPatch):
    """Draw arrows in 3D"""
    def __init__(self, xs, ys, zs, *args, **kwargs):
        FancyArrowPatch.__init__(self, (0,0), (0,0), *args, **kwargs)
        self._verts3d = xs, ys, zs

    def draw(self, renderer):
        xs3d, ys3d, zs3d = self._verts3d
        xs, ys, zs = proj3d.proj_transform(xs3d, ys3d, zs3d, renderer.M)
        self.set_positions((xs[0],ys[0]),(xs[1],ys[1]))
        FancyArrowPatch.draw(self, renderer)


fig = plt.figure()
ax = fig.gca(projection='3d')

cblue = '#1f77b4'
cgreen = '#2ca02c'
corange = '#ff7f0e'

y_lim = 1.5

dE = 0.4
# Plot a sin curve using the x and y axes.
x = np.linspace(0, 1, 100)
y = -np.cos(x * 4 * np.pi) / 2
def Eq(x):
    return dE + 2.5*(x)**2

def Eq_2D(x,y):
    return dE + 2.5*(x)**2 + 2.5*(y)**2


hv_y = 0.9 * y[0]      
hv = Arrow3D([0,0], [hv_y ,hv_y], [Eq(hv_y),0], 
                mutation_scale = 20, lw = 2, arrowstyle="<-", 
                color = "orange") 

ax.add_artist(hv)


F_y = y_lim   
F = Arrow3D([0,0], [F_y ,F_y ], [1,0], 
                mutation_scale = 20, lw = 1.75, arrowstyle="<-", 
                color = "black") 

ax.add_artist(F)

E = Arrow3D([0,0], [0 ,0], [1.2,-0.2], 
                mutation_scale = 20, lw = 1.75, arrowstyle="<-", 
                color = "black") 

ax.add_artist(E)


Q = Arrow3D([0,0], [0.75 ,-0.75], [0,0], 
                mutation_scale = 20, lw = 1.75, arrowstyle="<-", 
                color = "black") 

ax.add_artist(Q)

Q_dashed = Arrow3D([0,0], [y_lim ,-1], [0,0], 
                mutation_scale = 20, lw = 1.75, arrowstyle="-", 
                color = "black", linestyle="--") 

ax.add_artist(Q_dashed)


t = Arrow3D([1.2,0], [0 ,0], [0,0], 
                mutation_scale = 20, lw = 1.75, arrowstyle="<|-", 
                color = "black") 

ax.add_artist(t)

t = Arrow3D([1.2,0], [y_lim ,y_lim], [0,0], 
                mutation_scale = 20, lw = 1.75, arrowstyle="-", 
                color = "black", linestyle="--") 

ax.add_artist(t)

Q_flat = Arrow3D([0,0], [0.0 ,-0.8], [0,0], 
                mutation_scale = 20, lw = 3, arrowstyle="-", 
                color = cblue, linestyle="-") 

ax.add_artist(Q_flat)

# draw q(t)
ax.plot(x, 0.9*y, zs=0, zdir='z', label='Q', color='k')
# draw F(t)
ax.plot(x, -0.75*y, zs=y_lim, zdir='y', label='F', color= 'r') #corange)
# draw energy curve
x_lim = hv_y
dE = 0.4
x = np.linspace(-x_lim*1.1, x_lim*1.1 ,100)

X,Y = np.meshgrid(x, y)
Z_mesh = Eq_2D(X,Y)
Z = Z_mesh.reshape(len(x),len(y))
ax.plot(X, Y, zs= Z, zdir='z', color=cblue, linewidth  = 3)

#ax.plot(x, Eq(x), zs=0, zdir='x', color=cblue, linewidth  = 3)
# draw energy levels
#for x_l in (x_lim/3, 2*x_lim/3, x_lim):
#    x_l = np.array([-x_l, x_l])
#    ax.plot(x_l, Eq(x_l), zs=0, zdir='x', color=cblue, linewidth = 3)


ax.plot([0],[x_lim], 'o', zs=0, zdir='z',  color='m') 

# régle la fontsize dans le .mpl file
ax.text(0, 0.1, 1.2 ,"E$_\mathrm{Q}$", fontsize=20)
ax.text(0, 1.6, 0.92 ,"F$_\mathrm{Q}$", fontsize=20)
ax.text(1, -0.2, -0.2 ,"time", fontsize=20)  
ax.text(0, 0.3, .3 ,"Q$_\mathrm{1 THz}$", fontsize=20)   
# Make legend, set axes limits and labels
ax.set_xlim(0, 1)
ax.set_ylim(-1, 1)
ax.set_zlim(0, 1)
ax.set_xlabel('X')
ax.set_ylabel('Y')
ax.set_zlabel('Z')

# Turn the axis off
plt.axis('off')
# Customize the view angle so it's easier to see that the scatter points lie
# on the plane y=0
ax.view_init(elev=30., azim=-45)

plt.show
plt.savefig('cartoon_ellipse.png', dpi = 300, transparent = False)