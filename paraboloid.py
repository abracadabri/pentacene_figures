# -*- coding: utf-8 -*-
"""
Created on Wed Nov 18 10:25:00 2020

@author: seiler_admin
"""

import matplotlib.pyplot as plt
from matplotlib import cm
import numpy as np



# Create the surface
radius = 2
hole_radius = 0

# Generate the grid in cylindrical coordinates
r  = np.linspace(0, radius, 100)
theta = np.linspace(0, 2 * np.pi, 100)
R, THETA = np.meshgrid(r, theta)

X, Y = R * np.cos(THETA), R * np.sin(THETA)
a=0.5;b=0.6;c=0.6
Z1 = (X/a)**2+(Y/b)**2 # Elliptic paraboloid

# Do not plot the inner region
x = np.where(X**2+Y**2<=hole_radius**2,np.NAN,X)
y = np.where(X**2+Y**2<=hole_radius**2,np.NAN,Y)

# Plot the surface
fig = plt.figure()
ax = fig.gca(projection='3d')

ax.plot_surface(x, y, Z1, cmap=cm.coolwarm, linewidth=0, antialiased=True, cstride=2, rstride=2)

ax.set_xlabel("X")
ax.set_ylabel("Y")
ax.set_zlabel("Z")
plt.axis('off')
ax.view_init(elev = 0., azim=-55)
plt.savefig('paraboloid.png', transparent = False, dpi = 400)
plt.show()