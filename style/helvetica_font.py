# -*- coding: utf-8 -*-
"""
Created on Fri Nov 09 13:46:23 2018

@author: pfduc
"""
import os
from matplotlib import font_manager as fm, rcParams


def font_prop(ft_fname="HelveticaNeue.ttf", size=None, default_path=False):
    """load specific font from custom files"""
    if default_path:
        fpath = os.path.join(rcParams["datapath"], "fonts", "ttf", ft_fname)
    else:
        fpath = os.path.join(os.path.abspath(os.path.curdir), "style", ft_fname)
    font_p = fm.FontProperties(fname=fpath)
    if size is not None and isinstance(size, int):
        font_p.set_size(size)
    return font_p
