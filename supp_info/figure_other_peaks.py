# -*- coding: utf-8 -*-
"""
Created on Thu Jun 11 09:34:53 2020

@author: HEL
"""


from os.path import join, curdir
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
from matplotlib.collections import PatchCollection
import matplotlib.patches as patches
from mpl_toolkits.axisartist.axislines import SubplotZero
from matplotlib.patches import Circle, Rectangle
import matplotlib.image as mpimg
import seaborn as sns
import scipy.io as spio
#from ase.build import bulk
import crystals as crystals
from scipy import ndimage
from matplotlib import rcParams
import matplotlib.ticker as plticker
from mpl_toolkits.axes_grid1.inset_locator import inset_axes


plt.close()
directory  = r'C://Users//seiler_admin//Documents//Papers//Pentacene_single_crystal//pentacene_figures'
plt.style.use(join(directory, "style", "nature_com_presentation.mplstyle"))

blues = sns.color_palette("GnBu_d", 6)
reds =  sns.color_palette("Reds", 3)
oranges = sns.color_palette("YlOrBr", 6)
colors_peaks = [blues[3], blues[0], oranges[0], oranges[1], oranges[2], oranges[3], 'orange'] #reds #[blues[0], blues[3], 'orange', reds[7], 'grey']
labels = ['($0\\overline{2}0$)', '($020$)', '($2\\overline{3}0$)', '$620$']

labels = ['($\overline{12}0$)','120','1-20', '-120','-1-10', '110','-110','1-10','($020$)','($0\overline{2}0$)','($1\overline{3}0$)','(1$\overline{4}$0)','($\overline{1}30$)','($\overline{1}40$)',
          '($2\overline{1}0)$','2-20','($2\overline{3}0$)','2-40','2-50','($\overline{2}10$)','-220','($\overline{2}30$)','-240','-250',
          '3-10','3-30','-310','-330','high q']



def fig_other_peaks(ax, datapath=r"C:\\Users\\seiler_admin\\Documents\\Papers\\Pentacene_single_crystal\\pentacene_figures\\data"):
    dat = np.loadtxt(datapath + '/20200303_meas3_transients_from_fitted_Helene.txt')
    time = dat[:,0]
    a = dat[:,1:]
    
    N = 4
    ac = np.zeros((len(time)-N +1 ,len(a[0,:])))
    for i in range(len(a[0,:])):
        ac[:,i] = np.convolve(a[:,i], np.ones((N,))/N, mode='valid')
    timec = time[0:-N +1]
    #error = dat[:,2::2] 
    
    offset_t = 0.2
    offset = [0.18, 0.14, 0.12, 0.07, 0.05, -0.01, -0.03]
    offset_text = 7
    
    idx1 = 9
    ax.plot(timec + offset_t, ac[:,9] + offset[0], label  = '020', color = colors_peaks[0],  marker = 'o', markerfacecolor = 'None', zorder = 0)
    ax.text(offset_text, 1.16, labels[idx1], color = colors_peaks[0])
    
    #ax.fill_between(time,a[:,9]  - error[:,9], a[:,9]  + error[:,9], alpha = 0.2, color = colors[1])
    
    idx2 = 11
    ax.plot(timec + offset_t, ac[:,idx2] + offset[1] , label  = '230', color = colors_peaks[1], marker = 'd', markerfacecolor = 'None', zorder = 0)
    ax.text(offset_text, 1.14, labels[idx2], color = colors_peaks[1])
    #ax.fill_between(time,a[:,11]  - error[:,11], a[:,11]  + error[:,11], alpha = 0.2, color = colors[2])
    
    idx3 = 13
    ax.plot(timec + offset_t, ac[:,idx3] + offset[2], label  = '350', color = colors_peaks[2], marker = 's', markerfacecolor = 'None', zorder = 0)
    ax.text(offset_text, 1.08, labels[idx3], color = colors_peaks[2])
    #ax.fill_between(time,a[:,16]  - error[:,16], a[:,16]  + error[:,16], alpha = 0.2, color = colors[5])
    
    idx4 = 14
    ax.plot(timec + offset_t, ac[:,idx4] + offset[3], label  = '350', color = colors_peaks[3], marker = 'p', markerfacecolor = 'None', zorder = 0)
    ax.text(offset_text, 1.04, labels[idx4], color = colors_peaks[3])
    
    idx5 = 16
    ax.plot(timec + offset_t, ac[:,idx5] + offset[4], label  = '350', color = colors_peaks[4], marker = 'p', markerfacecolor = 'None', zorder = 0)
    ax.text(offset_text, 1.01, labels[idx5], color = colors_peaks[4])
    
    idx6 = 19
    ax.plot(timec + offset_t, ac[:,idx6] + offset[5], label  = '350', color = colors_peaks[5], marker = 'p', markerfacecolor = 'None', zorder = 0)
    ax.text(offset_text,0.99, labels[idx6], color = colors_peaks[5])
    
    idx7 = 21
    ax.plot(timec + offset_t, ac[:,idx7] + offset[6], label  = '350', color = colors_peaks[6], marker = 'p', markerfacecolor = 'None', zorder = 0)
    ax.text(offset_text,0.935, labels[idx7], color = colors_peaks[6])
    #ax.fill_between(time,a[:,idx4]  - error[:,idx4], a[:,idx4]  + error[:,idx4], alpha = 0.2, color = colors[8])
    
    
    ax.plot([0, 0], [0, 1.3], linestyle = 'dashed', linewidth = 0.5, color = 'grey')
    #ax.plot([0.6, 0.6], [0, 1.3], linestyle = 'dashed', linewidth = 0.5, color = 'grey')
    #ax.plot([1.2, 1.2], [0, 1.3], linestyle = 'dashed', linewidth = 0.5, color = 'grey')
    #ax.plot([-10, 300], [1, 1], linestyle = 'dashed', linewidth = 0.5, color = 'grey')
    ax.set_ylim(0.93, 1.2)
    ax.set_xlim(-1.5, 8)
    #ax.yaxis.set_label_coords(-0.12,0.5)
    #ax.xaxis.set_label_coords(0.5,-0.13)
    
    loc = plticker.MultipleLocator(base=1) # this locator puts ticks at regular intervals
    ax.xaxis.set_minor_locator(loc)
    loc = plticker.MultipleLocator(base=0.01) # this locator puts ticks at regular intervals
    ax.yaxis.set_minor_locator(loc)
    
    ax.set_xlabel('Time [ps]')
    ax.set_ylabel('Relative Intensity [a.u.]')
    
    
def fig_other_peaks_UP(ax, datapath=r"C:\\Users\\seiler_admin\\Documents\\Papers\\Pentacene_single_crystal\\pentacene_figures\\data"):
    dat = np.loadtxt(datapath + '/20200303_meas3_transients_from_fitted_Helene.txt')
    time = dat[:,0]
    a = dat[:,1:]
    
    print(len(time))
    N = 3
    ac = np.zeros((len(time)-N +1 ,len(a[0,:])))
    for i in range(len(a[0,:])):
        ac[:,i] = np.convolve(a[:,i], np.ones((N,))/N, mode='valid')
    timec = time[0:-N +1]
    #error = dat[:,2::2] 
    
    offset_t = 0.2
    offset = [0.18, 0.14, 0.10, 0.07, 0.02, -0.01, -0.03]
    offset_text = 6 #7
    
    idx1 = 8
    ax.plot(timec + offset_t, ac[:,idx1] + offset[0], label  = '020', color = colors_peaks[0],  marker = 'o', markerfacecolor = 'None', zorder = 0)
    ax.text(offset_text, 1.16, labels[idx1], color = colors_peaks[0])
    
    #ax.fill_between(time,a[:,9]  - error[:,9], a[:,9]  + error[:,9], alpha = 0.2, color = colors[1])
    
    idx2 = 9
    ax.plot(timec + offset_t, ac[:,idx2] + offset[1] , label  = '230', color = colors_peaks[1], marker = 'd', markerfacecolor = 'None', zorder = 0)
    ax.text(offset_text, 1.14, labels[idx2], color = colors_peaks[1])
    #ax.fill_between(time,a[:,11]  - error[:,11], a[:,11]  + error[:,11], alpha = 0.2, color = colors[2])
    
    idx3 = 11
    ax.plot(timec + offset_t, ac[:,idx3] + offset[2], label  = '350', color = colors_peaks[2], marker = 's', markerfacecolor = 'None', zorder = 0)
    ax.text(offset_text, 1.08, labels[idx3], color = colors_peaks[2])
    #ax.fill_between(time,a[:,16]  - error[:,16], a[:,16]  + error[:,16], alpha = 0.2, color = colors[5])
    
    idx4 = 13
    ax.plot(timec + offset_t, ac[:,idx4] + offset[3], label  = '350', color = colors_peaks[3], marker = 'p', markerfacecolor = 'None', zorder = 0)
    ax.text(offset_text, 1.04, labels[idx4], color = colors_peaks[3])
    
    idx5 = 14
    ax.plot(timec + offset_t, ac[:,idx5] + offset[4], label  = '350', color = colors_peaks[4], marker = 'p', markerfacecolor = 'None', zorder = 0)
    ax.text(offset_text, 1.01, labels[idx5], color = colors_peaks[4])
    
    idx6 = 19
    ax.plot(timec + offset_t, ac[:,idx6] + offset[5], label  = '350', color = colors_peaks[5], marker = 'p', markerfacecolor = 'None', zorder = 0)
    ax.text(offset_text,0.99, labels[idx6], color = colors_peaks[5])
    
    idx7 = 16
    ax.plot(timec + offset_t, ac[:,idx7] + offset[6], label  = '350', color = colors_peaks[6], marker = 'p', markerfacecolor = 'None', zorder = 0)
    ax.text(offset_text,0.935, labels[idx7], color = colors_peaks[6])
    #ax.fill_between(time,a[:,idx4]  - error[:,idx4], a[:,idx4]  + error[:,idx4], alpha = 0.2, color = colors[8])
    
    
    ax.plot([0, 0], [0, 1.3], linestyle = 'dashed', linewidth = 0.5, color = 'grey')
    #ax.plot([0.6, 0.6], [0, 1.3], linestyle = 'dashed', linewidth = 0.5, color = 'grey')
    #ax.plot([1.2, 1.2], [0, 1.3], linestyle = 'dashed', linewidth = 0.5, color = 'grey')
    #ax.plot([-10, 300], [1, 1], linestyle = 'dashed', linewidth = 0.5, color = 'grey')
    ax.set_ylim(0.93, 1.2)
    ax.set_xlim(-1, 8)
    #ax.yaxis.set_label_coords(-0.12,0.5)
    #ax.xaxis.set_label_coords(0.5,-0.13)
    
    loc = plticker.MultipleLocator(base=1) # this locator puts ticks at regular intervals
    ax.xaxis.set_minor_locator(loc)
    loc = plticker.MultipleLocator(base=0.01) # this locator puts ticks at regular intervals
    ax.yaxis.set_minor_locator(loc)
    
    ax.set_xlabel('Time [ps]')
    ax.set_ylabel('Relative Intensity [a.u.]')
    
w = 4
l = 4
fig = plt.figure(constrained_layout=False, figsize=(w, l))
ax = plt.gca()       
fig_other_peaks_UP(ax)
#plt.subplots_adjust(bottom = 0.15, right = 0.05)
plt.savefig('other_peaks_test_colours.pdf', dpi = 600)