# -*- coding: utf-8 -*-
"""
Created on Sun Nov 22 18:42:43 2020

@author: seiler_admin
"""
from os.path import join, curdir
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
from matplotlib.collections import PatchCollection
import matplotlib.patches as patches
from mpl_toolkits.axisartist.axislines import SubplotZero
from matplotlib.patches import Circle, Rectangle
import matplotlib.image as mpimg
#from style import dark_red, light_red, dark_blue, light_blue
import seaborn as sns
import scipy.io as spio
import crystals as crystals
from scipy import ndimage
from matplotlib import rcParams
#from utils import add_label
import matplotlib.ticker as plticker
from mpl_toolkits.axes_grid1.inset_locator import inset_axes
from PIL import Image


plt.close()
directory  = r'C://Users//seiler_admin//Documents//Papers//Pentacene_single_crystal//pentacene_figures'
plt.style.use(join(directory, "style", "nature_com.mplstyle"))

blues = sns.color_palette("GnBu_d", 6)
reds =  sns.color_palette("Reds", 9)
purples = sns.cubehelix_palette(8)
colors_peaks = [blues[0], blues[3], 'k', 'grey', 'orange', reds[4], reds[8],'green', 'grey']
labels = ['(020)','(0$\overline{2}$0)', '(1$\overline{1}$0)', '(110)', '(230)','(450)', '(530)']

path = "C://Users//seiler_admin//Documents//Papers//Pentacene_single_crystal//pentacene_figures//data"
def fig_2b(ax, datapath="data", label = 'title'):

    e_2D_vdos = np.load(datapath)
    #e_2d_z_vdos_inset = np.load(datapath + '/e_2d_z_vdos_inlet.npz')
    pcm = ax.pcolormesh(e_2D_vdos['f'], e_2D_vdos['f'], np.flipud(e_2D_vdos['vods']), cmap  = 'RdBu_r', vmin = -0.5, vmax = 0.5, rasterized = True)
    ax.axis('square')
    ax.set_xlim(0,15)
    ax.set_ylim(0,15)
    ax.set_xlabel('Frequency [THz]')
    ax.xaxis.set_label_coords(0.5, -0.15)
    ax.yaxis.set_label_coords(-0.15, 0.5)
    ax.set_ylabel('Frequency [THz]')
    loc = plticker.MultipleLocator(base=2) # this locator puts ticks at regular intervals
    ax.xaxis.set_major_locator(loc)
    ax.yaxis.set_major_locator(loc)
    ax.tick_params(direction='in', length=3, width=1, colors='k')
    ax.plot([0.75, 0.75], [0,15], linestyle = 'dashed', linewidth = 0.5, color = 'grey')
    ax.set_title(label)
    return pcm
    
def figure_3_2D_vDOS():
    w = 0.6*3.37
    l = 0.65*3*3.37
    fig = plt.figure(constrained_layout=False, figsize=(w, l))
    
    
    gs0 = fig.add_gridspec(
            3,
            1,
            height_ratios=[1, 1, 1],
            bottom=0.05,
            top=0.98,
            left=0.2,
            right=0.85,
            hspace=0.1,
            wspace = .1
        )
    ax_a1 = fig.add_subplot(gs0[0])
    ax_a2 = fig.add_subplot(gs0[1])

    ax_a3 = fig.add_subplot(gs0[2])
    #ax_a4 = fig.add_subplot(gs0[3])


    #fig_2k(ax_a1, f, vDOS_cuts['Y4ThzZ'], label = '$\phi_{\mathrm{YZ}}(4, \omega_Z)$')

    pcm1 = fig_2b(ax_a1, datapath = path + '//2dvdoszx.npz', label = '2D-vDOS: Z-X')
    pcm2 = fig_2b(ax_a2, datapath = path + '//2dvdoszy.npz', label = '2D-vDOS: Z-Y')
    pcm3 = fig_2b(ax_a3, datapath = path + '//2dvdoszz.npz', label = '2D-vDOS: Z-Z')
    
    ax_a3.set_xlabel('Frequency [THz]')
    #ax_a1.yaxis.set_label_coords(-0.15, -0.0)
    ax_a3.xaxis.set_label_coords(0.5, -0.15)
    lratio = 0.15
    rratio = 0.9
    tratio = 0.95
    bratio = 0.05
    
    cbaxes = fig.add_axes([0.90, 0.75, 0.02, 0.2])
    cbar = plt.colorbar(pcm1, cax=cbaxes)
    
    cbaxes = fig.add_axes([0.90, 0.45, 0.02, 0.2])
    cbar = plt.colorbar(pcm2, cax=cbaxes)
    
    cbaxes = fig.add_axes([0.90, 0.25, 0.02, 0.2])
    cbar = plt.colorbar(pcm3, cax=cbaxes)
    
    plt.subplots_adjust(left=lratio, right=rratio, top=tratio, bottom=bratio)
    plt.savefig("2D_vDOS.svg", dpi = 200, transparent=False)
    
figure_3_2D_vDOS()