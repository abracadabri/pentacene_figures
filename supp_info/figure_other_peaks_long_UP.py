# -*- coding: utf-8 -*-
"""
Created on Tue Jul 21 15:37:59 2020

@author: HEL
"""

# -*- coding: utf-8 -*-
"""
Created on Thu Jun 11 09:34:53 2020

@author: HEL
"""


from os.path import join, curdir
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
from matplotlib.collections import PatchCollection
import matplotlib.patches as patches
from mpl_toolkits.axisartist.axislines import SubplotZero
from matplotlib.patches import Circle, Rectangle
import matplotlib.image as mpimg
import seaborn as sns
import scipy.io as spio
from ase.build import bulk
import crystals as crystals
from scipy import ndimage
from matplotlib import rcParams
import matplotlib.ticker as plticker
from mpl_toolkits.axes_grid1.inset_locator import inset_axes


plt.close()
directory  = r'C://Users//HEL//Documents//Papers//Pentacene_single_crystal//pentacene_figures'
plt.style.use(join(directory, "style", "nature_com.mplstyle"))

blues = sns.color_palette("GnBu_d", 4)
reds =  sns.color_palette("Reds", 3)
colors_peaks = blues[:2]  + [reds[2]] + ['green', 'k'] #[blues[0], blues[3], 'orange', reds[7], 'grey']
labels = ['($0\\overline{2}0$)', '($020$)', '($2\\overline{3}0$)', '$620$']

labels = ['-1-20','120','1-20', '-120','-1-10', '110','-110','1-10','020','0-20','-230','2-30','-310','3-10',
          '-4-50', '-4-30', '-5-30', '-5-10', '-6-20', '600', '510', '620', '530', '430', '450']



labels = ['($020$)', '($0\\overline{2}0$)', '($2\\overline{3}0$)', '($\\overline{3}10$)', '($3\\overline{1}0$)', '($\\overline{45}0$)', '($\\overline{43}0$)', '($\\overline{53}0$)', '($\\overline{51}0$)', '($\\overline{62}0$)', '($600$)', '($510$)', '($620$)', '($530$)', '($430$)', '($450$)']



def fig_other_peaks_UP(ax, datapath=r"C:\\Users\\HEL\\Documents\\Papers\\Pentacene_single_crystal\\pentacene_figures\\data"):
    dat = np.loadtxt(datapath + '/20200225_global_fit_raw_dat.txt')
    fit = np.loadtxt(datapath + '/20200225_global_fit.txt')
    time = dat[:,0]
    a = dat[:,1:]
    af = fit[:,1:] + 1
    N = 2
    ac = np.zeros((len(time)-N +1 ,len(a[0,:])))
    for i in range(len(a[0,:])):
        ac[:,i] = np.convolve(a[:,i], np.ones((N,))/N, mode='valid')
    timec = time[0:-N +1]
    #error = dat[:,2::2] 
    
    offset_t = 0.2
    offset = [0., 0., 0., 0., 0., -0.0, -0.0]
    offset_text = 260
    
    ax.plot([-10, 300], [1, 1], linestyle = 'dashed', linewidth = 0.5, color = 'grey')
    
    idx1 = 0
    ax.plot(timec + offset_t, ac[:,idx1] + offset[0], label  = '020', color = colors_peaks[0],  marker = 'o', markerfacecolor = 'None', linestyle = 'None')
    ax.plot(time, af[:,idx1] + offset[0], label  = '020', color = colors_peaks[0],  marker = 'o', markerfacecolor = 'None', zorder = 0)
    ax.text(offset_text, 1.06, labels[idx1], color = colors_peaks[0])
    
    #ax.fill_between(time,a[:,9]  - error[:,9], a[:,9]  + error[:,9], alpha = 0.2, color = colors[1])
    
    idx2 = 1
    ax.plot(timec + offset_t, ac[:,idx2] + offset[1] , label  = '230', color = colors_peaks[1], marker = 'd', markerfacecolor = 'None', linestyle = 'None')
    ax.plot(time, af[:,idx2] + offset[1], label  = '020', color = colors_peaks[1],  marker = 'o', markerfacecolor = 'None', zorder = 0)
    ax.text(offset_text, 1.02, labels[idx2], color = colors_peaks[1])
    #ax.fill_between(time,a[:,11]  - error[:,11], a[:,11]  + error[:,11], alpha = 0.2, color = colors[2])
    
    idx3 = 2
    ax.plot(timec + offset_t, ac[:,idx3] + offset[2], label  = '350', color = colors_peaks[2], marker = 's', markerfacecolor = 'None', zorder = 0, linestyle = 'None')
    ax.plot(time, af[:,idx3] + offset[2], label  = '020', color = colors_peaks[2],  marker = 'o', markerfacecolor = 'None', zorder = 0)
    ax.text(offset_text, 0.96, labels[idx3], color = colors_peaks[2])
    #ax.fill_between(time,a[:,16]  - error[:,16], a[:,16]  + error[:,16], alpha = 0.2, color = colors[5])
    
    idx4 = 12
    ax.plot(timec + offset_t, ac[:,idx4] + offset[3], label  = '350', color = colors_peaks[3], marker = 'p', markerfacecolor = 'None', zorder = 0, linestyle = 'None')
    ax.plot(time, af[:,idx4] + offset[4], label  = '020', color = colors_peaks[3],  marker = 'o', markerfacecolor = 'None', zorder = 0)
    ax.text(offset_text, 0.86, labels[idx4], color = colors_peaks[3])
    
    idx5 = 8
    ax.plot(timec + offset_t, ac[:,idx5] + offset[4], label  = '350', color = colors_peaks[4], marker = 'p', markerfacecolor = 'None', zorder = 0, linestyle = 'None')
    ax.plot(time, af[:,idx5] + offset[4], label  = '020', color = colors_peaks[4],  marker = 'o', markerfacecolor = 'None', zorder = 0)
    ax.text(offset_text, 0.92, labels[idx5], color = colors_peaks[4])

    #ax.fill_between(time,a[:,idx4]  - error[:,idx4], a[:,idx4]  + error[:,idx4], alpha = 0.2, color = colors[8])
    
    
    ax.plot([-10, 300], [1, 1], linestyle = 'dashed', linewidth = 0.5, color = 'grey')
    #ax.plot([0.6, 0.6], [0, 1.3], linestyle = 'dashed', linewidth = 0.5, color = 'grey')
    #ax.plot([1.2, 1.2], [0, 1.3], linestyle = 'dashed', linewidth = 0.5, color = 'grey')
    #ax.plot([-10, 300], [1, 1], linestyle = 'dashed', linewidth = 0.5, color = 'grey')
    ax.set_ylim(0.82, 1.08)
    ax.set_xlim(-10,300)
    #ax.yaxis.set_label_coords(-0.12,0.5)
    #ax.xaxis.set_label_coords(0.5,-0.13)
    
    loc = plticker.MultipleLocator(base=25) # this locator puts ticks at regular intervals
    ax.xaxis.set_minor_locator(loc)
    loc = plticker.MultipleLocator(base=0.01) # this locator puts ticks at regular intervals
    ax.yaxis.set_minor_locator(loc)
    
    ax.set_xlabel('Time [ps]')
    ax.set_ylabel('Relative Intensity [a.u.]')
    
w = 6
l = 3.5
fig = plt.figure(constrained_layout=False, figsize=(w, l))
ax = plt.gca()       
fig_other_peaks_UP(ax)
#plt.subplots_adjust(bottom = 0.15, right = 0.05)
plt.savefig('other_peaks_UP_long.pdf', dpi = 600)