# -*- coding: utf-8 -*-
"""
Created on Thu May 28 18:51:38 2020

@author: HEL
"""

from os.path import join, curdir
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
from matplotlib.collections import PatchCollection
import matplotlib.patches as patches
from mpl_toolkits.axisartist.axislines import SubplotZero
from matplotlib.patches import Circle, Rectangle
import matplotlib.image as mpimg
#from style import dark_red, light_red, dark_blue, light_blue
import seaborn as sns
import scipy.io as spio
#from ase.build import bulk
import crystals as crystals
from scipy import ndimage
from matplotlib import rcParams
#from utils import add_label
import matplotlib.ticker as plticker
from mpl_toolkits.axes_grid1.inset_locator import inset_axes
from PIL import Image


plt.close()
directory  = r'C://Users//seiler_admin//Documents//Papers//Pentacene_single_crystal//pentacene_figures'
plt.style.use(join(directory, "style", "nature_com.mplstyle"))

blues = sns.color_palette("GnBu_d", 6)
reds =  sns.color_palette("Reds", 9)
purples = sns.cubehelix_palette(8)
colors_peaks = [blues[0], blues[3], 'k', 'grey', 'orange', reds[4], reds[8],'green', 'grey']
labels = ['(020)','(0$\overline{2}$0)', '(1$\overline{1}$0)', '(110)', '(230)','(450)', '(530)']


def fig_DFT_modes_activated(ax, datapath="C://Users//seiler_admin//Documents//Papers//Pentacene_single_crystal//pentacene_figures//data"):
    TDDFT = np.load(datapath +'/TDDFT_a_spin.npz') 
    freq_nor = np.transpose(TDDFT['factors']/np.max(TDDFT['factors']))
    t = TDDFT['time']
    f = TDDFT['freq']
    #along z, not spin calculations
    idx = [1,2,4,12,13,26,27,28]
    #along x, spin pol calculations
    idx = [1,4, 6, 9,12,13,17,18,27]
    print(f[5])
    colors = ['b', 'g', blues[0], 'r', 'orange', 'k', 'grey', reds[4], reds[8]]
    for j,i in enumerate(idx):   
        ax.plot(t, freq_nor[i,:], color = colors[j], label = '{:04.2f}'.format(f[i]) + ' THz', linewidth = 2)

    ax.set_xlim(0, 48)
    ax.set_ylim(0,1)
    ax.legend(fontsize = 10)
    ax.set_xlabel('Time [fs]')
    loc = plticker.MultipleLocator(base=0.2) # this locator puts ticks at regular intervals
    ax.yaxis.set_major_locator(loc)
    ax.xaxis.set_label_coords(0.5, -0.1)
    ax.yaxis.set_label_coords(-0.1, 0.5)
    ax.set_ylabel('Rel. mag. of mode coeff.')
    
w = 1.5*3.37
l = 3.37
fig = plt.figure(constrained_layout=False, figsize=(w, l))
ax = plt.gca()       
fig_DFT_modes_activated(ax)
#fig_2a(ax)
#fig_2b(ax)
lratio = 0.15
rratio = 0.95
tratio = 0.93
bratio = 0.15
plt.subplots_adjust(left=lratio, right=rratio, top=tratio, bottom=bratio)
plt.savefig('TDDFT_mode_activated_spin.svg', dpi = 400, transparent = True)