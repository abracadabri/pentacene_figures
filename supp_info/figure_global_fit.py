# -*- coding: utf-8 -*-
"""
Created on Wed Aug 19 12:02:27 2020

@author: HEL
"""

# -*- coding: utf-8 -*-
"""
Created on Thu Jun 11 09:34:53 2020

@author: HEL
"""


from os.path import join, curdir
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
from matplotlib.collections import PatchCollection
import matplotlib.patches as patches
from mpl_toolkits.axisartist.axislines import SubplotZero
from matplotlib.patches import Circle, Rectangle
import matplotlib.image as mpimg
import seaborn as sns
import scipy.io as spio
#from ase.build import bulk
import crystals as crystals
from scipy import ndimage
from matplotlib import rcParams
import matplotlib.ticker as plticker
from mpl_toolkits.axes_grid1.inset_locator import inset_axes


plt.close()
directory  = r'C://Users//seiler_admin//Documents//Papers//Pentacene_single_crystal//pentacene_figures'
plt.style.use(join(directory, "style", "nature_com.mplstyle"))

blues = sns.color_palette("GnBu_d", 4)
reds =  sns.color_palette("Reds", 3)
colors_peaks = blues  + reds #[blues[0], blues[3], 'orange', reds[7], 'grey']
labels = ['($0\\overline{2}0$)', '($020$)', '($2\\overline{3}0$)', '$620$']

colors = sns.color_palette("hls", 17)
labels = ['($\overline{12}0$)','120','1-20', '-120','-1-10', '110','-110','1-10','($020$)','($0\overline{2}0$)','($1\overline{3}0$)','(1$\overline{4}$0)','($\overline{1}30$)','($\overline{1}40$)',
          '($2\overline{1}0)$','2-20','($2\overline{3}0$)','2-40','2-50','($\overline{2}10$)','-220','($\overline{2}30$)','-240','-250',
          '3-10','3-30','-310','-330','high q']

blues = sns.color_palette("GnBu_d", 6)
reds =  sns.color_palette("Reds", 9)
colors = [blues[0], blues[3], 'orange', 'grey', 'grey','grey','grey',reds[8], 'grey',  'grey' , 'grey' , 'grey' , 'grey' , 'grey' ,  'grey', reds[4], 'grey']
print(len(colors))
str_label = '($020$) ($0\overline{2}0$) ($2\overline{3}0$) ($\overline{3}10$) ($3\overline{1}0$) ($\overline{45}0$) ($\overline{43}0$) ($\overline{53}0$) ($\overline{51}0$) ($\overline{62}0$) ($600$) ($510$) ($620$) ($530$) ($430$) ($450$)'
labels = str_label.split(' ')

def fig_other_peaks(ax, datapath=r"C:\\Users\\seiler_admin\\Documents\\Papers\\Pentacene_single_crystal\\pentacene_figures\\data"):
    dat = np.loadtxt(datapath + '/20200225_global_fit.txt')
    dat_raw = np.loadtxt(datapath + '/20200225_global_fit_raw_dat.txt')
    t = dat[:,0]
    for i in range(len(dat[0,1:])):
        y_fit = dat[:,i+1]
        plt.plot(t, dat_raw[:, i+1] -1 + 0.1*i, color = colors[i], marker = 'o', markersize = 3, linestyle = 'None', rasterized = False)
        ax.plot(t, y_fit +0.1*i, color = colors[i], rasterized = False)
        ax.text(250, 0.095*i, labels[i],color = colors[i], rasterized = False)
    ax.set_xlim(-10, 300)
    ax.set_ylim(-0.1, 1.55)
    ax.set_xlabel('Time [ps]')
    ax.set_ylabel('Relative Intensity [a.u.]')
    #ax.set_title('Global fit')

    
w = 4
l = 10
fig = plt.figure(constrained_layout=False, figsize=(w, l))
ax = plt.gca()       
fig_other_peaks(ax)
#plt.subplots_adjust(bottom = 0.15, right = 0.05)
plt.savefig('globalfit_long.svg', dpi = 600, transparent = True)