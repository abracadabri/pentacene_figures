# -*- coding: utf-8 -*-
"""
Created on Thu Jun 11 09:34:53 2020

@author: HEL
"""


from os.path import join, curdir
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
from matplotlib.collections import PatchCollection
import matplotlib.patches as patches
from mpl_toolkits.axisartist.axislines import SubplotZero
from matplotlib.patches import Circle, Rectangle
import matplotlib.image as mpimg
import seaborn as sns
import scipy.io as spio
#from ase.build import bulk
import crystals as crystals
from scipy import ndimage
from matplotlib import rcParams
import matplotlib.ticker as plticker
from mpl_toolkits.axes_grid1.inset_locator import inset_axes


plt.close()
directory  = r'C://Users//seiler_admin//Documents//Papers//Pentacene_single_crystal//pentacene_figures'
plt.style.use(join(directory, "style", "nature_com_presentation.mplstyle"))

blues = sns.color_palette("GnBu_d", 6)
reds =  sns.color_palette("Reds", 3)
oranges = sns.color_palette("YlOrBr", 9)
colors_peaks = [blues[0], blues[3], '#da467d', '#ffb2d0','k', 'grey', '#2baf6a', '#aefd6c', '#b790d4', 'purple', oranges[5], oranges[7], 'orange', oranges[1]] #reds #[blues[0], blues[3], 'orange', reds[7], 'grey']
labels = ['($0\\overline{2}0$)', '($020$)', '($2\\overline{3}0$)', '$620$']

labels = ['($\overline{12}0$)','($120$)','($1\overline{2}0$)', '($\overline{1}$20)','($\overline{11}0$)', '($110$)','($\overline{1}10$)','($1\overline{1}0$)','($020$)','($0\overline{2}0$)','($1\overline{3}0$)','(1$\overline{4}$0)','($\overline{1}30$)','($\overline{1}40$)',
          '($2\overline{1}0)$','2-20','($2\overline{3}0$)','2-40','2-50','($\overline{2}10$)','-220','($\overline{2}30$)','-240','-250',
          '3-10','3-30','-310','-330','high q']

def fig_2_peaks(ax, idx1, idx2, datapath=r"C:\\Users\\seiler_admin\\Documents\\Papers\\Pentacene_single_crystal\\pentacene_figures\\data"):
    dat = np.loadtxt(datapath + '/20200303_meas3_transients_from_fitted_Helene.txt')
    time = dat[:,0]
    a = dat[:,1:]
    N = 5
    ac = np.zeros((len(time)-N +1 ,len(a[0,:])))
    for i in range(len(a[0,:])):
        ac[:,i] = np.convolve(a[:,i], np.ones((N,))/N, mode='valid')
    timec = time[0:-N +1]
    offset_text = 4
    ax.plot(timec , ac[:,idx1] , label  = '020', color = colors_peaks[0],  marker = 'o', markerfacecolor = 'None', markersize = 4, zorder = 0)
    ax.text(offset_text, 0.995, labels[idx1], color = colors_peaks[0])
    
    ax.plot(timec, ac[:,idx2]  , label  = '230', color = colors_peaks[1], marker = 'd', markerfacecolor = 'None', markersize = 4,zorder = 0)
    ax.text(offset_text, 0.98, labels[idx2], color = colors_peaks[1])
    ax.plot([0, 0], [0, 1.3], linestyle = 'dashed', linewidth = 0.5, color = 'grey')

    ax.set_ylim(0.96, 1.03)
    ax.set_xlim(-1, 7)

    
    loc = plticker.MultipleLocator(base=1) # this locator puts ticks at regular intervals
    ax.xaxis.set_minor_locator(loc)
    loc = plticker.MultipleLocator(base=0.01) # this locator puts ticks at regular intervals
    ax.yaxis.set_minor_locator(loc)
    
    ax.set_xlabel('Time [ps]')
    ax.set_ylabel('Relative Intensity [a.u.]')
    
    
    
def fig_other_peaks_UP(ax, datapath=r"C:\\Users\\seiler_admin\\Documents\\Papers\\Pentacene_single_crystal\\pentacene_figures\\data"):
    dat = np.loadtxt(datapath + '/20200303_meas3_transients_from_fitted_Helene.txt')
    time = dat[:,0]
    a = dat[:,1:]
    
    print(len(time))
    N = 5
    ac = np.zeros((len(time)-N +1 ,len(a[0,:])))
    for i in range(len(a[0,:])):
        ac[:,i] = np.convolve(a[:,i], np.ones((N,))/N, mode='valid')
    timec = time[0:-N +1]
    #error = dat[:,2::2] 
    
    offset_t = 0.2
    offset = [0.18, 0.14, 0.10, 0.03, -0.02, -0.05, -0.09, -0.11, -0.15,-0.19, -0.25, -0.29, -0.33, -0.37]
    offset_text = 6 #7
    
    idx1 = 8
    ax.plot(timec + offset_t, ac[:,idx1] + offset[0], label  = '020', color = colors_peaks[0],  marker = 'o', markerfacecolor = 'None', markersize = 4, zorder = 0)
    ax.text(offset_text, 1.16, labels[idx1], color = colors_peaks[0])
    
    #ax.fill_between(time,a[:,9]  - error[:,9], a[:,9]  + error[:,9], alpha = 0.2, color = colors[1])
    
    idx2 = 9
    ax.plot(timec + offset_t, ac[:,idx2] + offset[1] , label  = '230', color = colors_peaks[1], marker = 'd', markerfacecolor = 'None', markersize = 4,zorder = 0)
    ax.text(offset_text, 1.14, labels[idx2], color = colors_peaks[1])
    #ax.fill_between(time,a[:,11]  - error[:,11], a[:,11]  + error[:,11], alpha = 0.2, color = colors[2])
    
    idx3 = 1
    ax.plot(timec + offset_t, ac[:,idx3] + offset[2], label  = '350', color = colors_peaks[2], marker = 's', markerfacecolor = 'None', markersize = 4,zorder = 0)
    ax.text(offset_text, 1.08, labels[idx3], color = colors_peaks[2])
    #ax.fill_between(time,a[:,16]  - error[:,16], a[:,16]  + error[:,16], alpha = 0.2, color = colors[5])
    
    idx4 = 3
    ax.plot(timec + offset_t, ac[:,idx4] + offset[3], label  = '350', color = colors_peaks[3], marker = 'p', markerfacecolor = 'None', markersize = 4,zorder = 0)
    ax.text(offset_text, 1.04, labels[idx4], color = colors_peaks[3])
    
    idx5 = 6
    ax.plot(timec + offset_t, ac[:,idx5] + offset[4], label  = '350', color = colors_peaks[4], marker = 'p', markerfacecolor = 'None', markersize = 4,zorder = 0)
    ax.text(offset_text, 1.01, labels[idx5], color = colors_peaks[4])
    
    idx6 = 7
    ax.plot(timec + offset_t, ac[:,idx6] + offset[5], label  = '350', color = colors_peaks[5], marker = 'p', markerfacecolor = 'None', markersize = 4,zorder = 0)
    ax.text(offset_text,0.99, labels[idx6], color = colors_peaks[5])
    
    idx7 = 10
    ax.plot(timec + offset_t, ac[:,idx7] + offset[6], label  = '350', color = colors_peaks[6], marker = 'p', markerfacecolor = 'None', markersize = 4,zorder = 0)
    ax.text(offset_text,0.935, labels[idx7], color = colors_peaks[6])
    
    idx8 = 12
    ax.plot(timec + offset_t, ac[:,idx8] + offset[7], label  = '350', color = colors_peaks[7], marker = 'p', markerfacecolor = 'None', markersize = 4,zorder = 0)
    ax.text(offset_text,0.90, labels[idx8], color = colors_peaks[7])
    
    idx9 = 11
    ax.plot(timec + offset_t, ac[:,idx9] + offset[8], label  = '350', color = colors_peaks[8], marker = 'p', markerfacecolor = 'None', markersize = 4,zorder = 0)
    ax.text(offset_text,0.85, labels[idx9], color = colors_peaks[8])
    
    idx10 = 13
    ax.plot(timec + offset_t, ac[:,idx10] + offset[9], label  = '350', color = colors_peaks[9], marker = 'p', markerfacecolor = 'None', markersize = 4,zorder = 0)
    ax.text(offset_text,0.80, labels[idx10], color = colors_peaks[9])
    
    idx11 = 14
    ax.plot(timec + offset_t, ac[:,idx11] + offset[10], label  = '350', color = colors_peaks[10], marker = 'p', markerfacecolor = 'None', markersize = 4,zorder = 0)
    ax.text(offset_text,0.75, labels[idx11], color = colors_peaks[10])
    
    idx12 = 19
    ax.plot(timec + offset_t, ac[:,idx12] + offset[11], label  = '350', color = colors_peaks[11], marker = 'p', markerfacecolor = 'None', markersize = 4,zorder = 0)
    ax.text(offset_text,0.7, labels[idx12], color = colors_peaks[11])
    
    #idx13 = 16
    #ax.plot(timec + offset_t, ac[:,idx13] + offset[12], label  = '350', color = colors_peaks[12], marker = 'p', markerfacecolor = 'None', markersize = 4,zorder = 0)
    #ax.text(offset_text,0.65, labels[idx13], color = colors_peaks[12])
    
    #idx14 = 21
    #ax.plot(timec + offset_t, ac[:,idx14] + offset[13], label  = '350', color = colors_peaks[13], marker = 'p', markerfacecolor = 'None', markersize = 4,zorder = 0)
    #ax.text(offset_text,0.6, labels[idx14], color = colors_peaks[13])
    
    #ax.fill_between(time,a[:,idx4]  - error[:,idx4], a[:,idx4]  + error[:,idx4], alpha = 0.2, color = colors[8])
    
    
    ax.plot([0, 0], [0, 1.3], linestyle = 'dashed', linewidth = 0.5, color = 'grey')
    #ax.plot([0.6, 0.6], [0, 1.3], linestyle = 'dashed', linewidth = 0.5, color = 'grey')
    #ax.plot([1.2, 1.2], [0, 1.3], linestyle = 'dashed', linewidth = 0.5, color = 'grey')
    #ax.plot([-10, 300], [1, 1], linestyle = 'dashed', linewidth = 0.5, color = 'grey')
    ax.set_ylim(0.67, 1.2)
    ax.set_xlim(-1, 8)
    #ax.yaxis.set_label_coords(-0.12,0.5)
    #ax.xaxis.set_label_coords(0.5,-0.13)
    
    loc = plticker.MultipleLocator(base=1) # this locator puts ticks at regular intervals
    ax.xaxis.set_minor_locator(loc)
    loc = plticker.MultipleLocator(base=0.01) # this locator puts ticks at regular intervals
    ax.yaxis.set_minor_locator(loc)
    
    ax.set_xlabel('Time [ps]')
    ax.set_ylabel('Relative Intensity [a.u.]')
    
w = 4
l = 8

fig = plt.figure(constrained_layout=False, figsize=(w, l))
ax = plt.gca()      
fig_other_peaks_UP(ax)
plt.savefig('other_peaks_test_colours2.pdf', dpi = 600, transparent = False)
#idx = [[0,1], [2,3],[4,5],[6,7],[8,9],[10,12],[11,13],[14,19],[16,21]]
#for i in idx:
#    fig = plt.figure(constrained_layout=False, figsize=(w, l))
#    ax = plt.gca() 
#    fig_2_peaks(ax, i[0],i[1])
    #plt.subplots_adjust(bottom = 0.15, right = 0.05)
#   plt.savefig('other_peaks_test_colours' + str(i[0]) + '.pdf', dpi = 600, transparent = False)