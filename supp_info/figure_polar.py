# -*- coding: utf-8 -*-
"""
Created on Thu May 28 18:51:38 2020

@author: HEL
"""

from os.path import join, curdir
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
from matplotlib.collections import PatchCollection
import matplotlib.patches as patches
from mpl_toolkits.axisartist.axislines import SubplotZero
from matplotlib.patches import Circle, Rectangle
import matplotlib.image as mpimg
import seaborn as sns
import scipy.io as spio
#from ase.build import bulk
import crystals as crystals
from scipy import ndimage
from matplotlib import rcParams
import matplotlib.ticker as plticker
from mpl_toolkits.axes_grid1.inset_locator import inset_axes


plt.close()
directory  = r'C://Users//seiler_admin//Documents//Papers//Pentacene_single_crystal//pentacene_figures'
plt.style.use(join(directory, "style", "nature_com.mplstyle"))

blues = sns.color_palette("GnBu_d", 6)
reds =  sns.color_palette("Reds", 9)
colors_peaks = [blues[3], blues[0], 'orange', reds[6], 'grey']
labels = ['($0\\overline{2}0$)', '($020$)', '($2\\overline{3}0$)', '($620$)']

def fig_pol(ax, datapath=r"C:\\Users\\seiler_admin\\Documents\\Papers\\Pentacene_single_crystal\\pentacene_figures\\data"):
    dat = np.loadtxt(datapath + '\\20200225_Meas7_polarization_scan.txt')
    angle = dat[:,0]
    a = dat[:,1:]
    #error = dat[:,2::2] 
    
    idx1 = 0
    ax.plot(angle, a[:,idx1] , label  = '020', color = colors_peaks[0],  marker = 'o', markerfacecolor = 'None', zorder = 0)
    ax.text(240, 0.85, labels[0], color = colors_peaks[0])
    
    idx2 = 1
    ax.plot(angle, a[:,idx2] + 0.1 , label  = '230', color = colors_peaks[1], marker = 'd', markerfacecolor = 'None', zorder = 0)
    #ax.fill_between(time,a[:,11]  - error[:,11], a[:,11]  + error[:,11], alpha = 0.2, color = colors[2])
    ax.text(240, 1, labels[1], color = colors_peaks[1])
    
    idx3 = 2
    ax.plot(angle, a[:,idx3] + 0.2 , label  = '350', color = colors_peaks[2], marker = 's', markerfacecolor = 'None', zorder = 0)
    ax.text(240, 1.1, labels[2], color = colors_peaks[2])
    #ax.fill_between(time,a[:,16]  - error[:,16], a[:,16]  + error[:,16], alpha = 0.2, color = colors[5])
    
    idx4 = 3
    ax.plot(angle, a[:,idx4] + 0.4 , label  = '350', color = colors_peaks[3], marker = 'p', markerfacecolor = 'None', zorder = 0)
    ax.text(240, 1.26, labels[3], color = colors_peaks[3])
    #ax.fill_between(time,a[:,idx4]  - error[:,idx4], a[:,idx4]  + error[:,idx4], alpha = 0.2, color = colors[8])
    

    #ax.set_ylim(0.96, 1.02)
    ax.set_xlim(-0, 350)
    #ax.yaxis.set_label_coords(-0.12,0.5)
    #ax.xaxis.set_label_coords(0.5,-0.13)
    
    loc = plticker.MultipleLocator(base=10) # this locator puts ticks at regular intervals
    ax.xaxis.set_minor_locator(loc)
    loc = plticker.MultipleLocator(base=0.05) # this locator puts ticks at regular intervals
    ax.yaxis.set_minor_locator(loc)
    
    ax.set_xlabel('Waveplate angle [°]')
    ax.set_ylabel('Relative Intensity [a.u.]')
    
w = 6
l = 3.2
fig = plt.figure(constrained_layout=False, figsize=(w, l))
ax = plt.gca()       
fig_pol(ax)
plt.subplots_adjust(bottom = 0.15, right = 0.95)
plt.savefig('polarization_scan.pdf', dpi = 600)