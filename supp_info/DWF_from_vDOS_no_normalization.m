function [msd,B] = DWF_from_vDOS_no_normalization(omega,vDOS,atomic_mass,T)
% calculates the Debye-Waller factor from the phonon DOS for simple
% crystals.
%
% inputs: omega in meV, vDOS, atomic_mass in u
% temperature range for the Debye-Waller-factor (array) [K]
% the vDOS doesn't get normalized here!!! needs to be states/meV/unit cell
%
% for further information see Peng et al., High-Energy Electron Diffraction
% and Microscopy


% constants:
kb=8.617*1e-5;  % Boltzmann constant, in eV/K
e=1.60217e-19;  % elementary charge, for conversion from eV to J
hbar=4.135e-15/(2*pi); % reduced Planck constant, in eVs
u=1.66053904*1e-27; %atomic mass unit, in kg


% calculate mean squared displacement from vDOS. (all in SI units)

% convert omega from meV to Hz
omega=omega*1e-3/hbar; %in Hz
% convert atomic mass to kg
m=atomic_mass*u; %atomic mass in kg
%convert vDOS to states/Hz/unit cell
vDOS=vDOS*1e3*hbar;

%prepare variables
msd=nan(size(T));
B=nan(size(T));

disp(coth(hbar*omega./(2*kb*T(1))))
figure()
plot(coth(hbar*omega./(2*kb*T(1))))

for j=1:length(T)
        msd(j)=3*hbar*e/(2*m)*trapz(omega,coth(hbar*omega./(2*kb*T(j))).*vDOS./omega)*1e20; %in Angstroem^2
        B(j)=8/3*pi^2*msd(j); %DWF, see Peng (=the electron diffraction bible)
end


end

