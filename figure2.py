# -*- coding: utf-8 -*-
"""
Created on Thu May 28 18:51:38 2020

@author: HEL
"""

from os.path import join, curdir
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
from matplotlib.collections import PatchCollection
import matplotlib.patches as patches
from mpl_toolkits.axisartist.axislines import SubplotZero
from matplotlib.patches import Circle, Rectangle
import matplotlib.image as mpimg
from style import dark_red, light_red, dark_blue, light_blue
import seaborn as sns
import scipy.io as spio
#from ase.build import bulk
import crystals as crystals
from scipy import ndimage
from matplotlib import rcParams
from utils import add_label
import matplotlib.ticker as plticker
from mpl_toolkits.axes_grid1.inset_locator import inset_axes
from PIL import Image


plt.close()
plt.style.use(join(curdir, "style", "nature_com.mplstyle"))

blues = sns.color_palette("GnBu_d", 6)
reds =  sns.color_palette("Reds", 9)
purples = sns.cubehelix_palette(8)
colors_peaks = [blues[0], blues[3], 'k', 'grey', 'orange', reds[4], reds[8],'green', 'grey']
labels = ['(020)','(0$\overline{2}$0)', '($\overline{1}$10)', '(1$\overline{1}$0)', '(230)','(450)', '(530)']


def fig_2a(ax, datapath="data"):
    peaks_subset = np.loadtxt(datapath + '/peaks_to_label.txt')

    d2 = np.load(datapath + '/difference_1andhalf_ps.npy')
    im = Image.fromarray(np.int16(d2))
    im = im.rotate(20)
    pcm = ax.pcolormesh(im, cmap  = 'RdBu_r', vmin = -1500, vmax = 1500, rasterized = True)
    

    
    labels = ['(020)','(0$\overline{2}$0)',  '($\overline{1}$10)', '(1$\overline{1}$0)','(2$\overline{3}$0)','(450)', '($\overline{53}$0)']
    shiftx = -85
    shifty = -80

    props = dict(boxstyle='round', facecolor='#cecece', alpha=0.2)
    ax.text(0.58, 0.93, 't = 1.5 ps', transform=ax.transAxes,
        verticalalignment='top', bbox = props)

    zero_order = Circle((190, 210), 30, color = 'white')
    ax.add_patch(zero_order)
    
    for idx, p in enumerate(peaks_subset):
        c = Circle((p[0] + shiftx, p[1]  + shifty), 20, edgecolor = colors_peaks[idx], linewidth = 1.5, facecolor = 'None')
        ax.add_patch(c)
        ax.plot(p[0], p[1] , marker = 'x', color = 'w')
        print(p[0], p[1])
        ax.text(p[0] + shiftx, p[1]  + shifty, labels[idx],  color = 'k', fontsize = 10)
        pass
    
    
    pcm.set_rasterized(True)
    ax.axis('square')
    ax.set_xlim(0, 400)
    ax.set_ylim(0, 400)
    ax.set_xticks([])
    ax.set_yticks([])
    
    
def fig_2b(ax, datapath="data"):
    peaks_subset = np.loadtxt(datapath + '/peaks_to_label.txt')

    d2 = np.load(datapath + '/difference_50ps.npy')
    pcm = ax.pcolormesh(d2, cmap  = 'RdBu_r', vmin = -3000, vmax = 3000, rasterized = True)
    
    labels = ['(020)','(0$\overline{2}$0)', '(1$\overline{1}$0)', '(110)', '(2$\overline{3}$0)','(450)', '($\overline{53}$0)']
    shiftx = 20
    shifty = 15
    
    props = dict(boxstyle='round', facecolor='#cecece', alpha=0.2)
    ax.text(0.58, 0.93, 't = 50 ps', transform=ax.transAxes,
            verticalalignment='top', bbox = props)
    
    #for idx, p in enumerate(peaks_subset):
        #c = Circle((p[0] + shiftx, p[1] + shifty), 20, edgecolor = colors_peaks[idx], linewidth = 1.5, facecolor = 'None')
        #ax.add_patch(c)
        #ax.plot(p[0], p[1] , marker = 'x', color = 'w')
        #print(p[0], p[1])
        #ax.text(p[0], p[1] , labels[idx],  color = 'k', fontsize = 12)
        #pass
    
    pcm.set_rasterized(True)
    ax.axis('square')
    ax.set_xlim(300 - 200, 300  +200)
    ax.set_ylim(300 - 200, 300  +200)
    zero_order = Circle((300, 300), 30, color = 'white')
    ax.add_patch(zero_order)
    
    ax.set_xticks([])
    ax.set_yticks([])
    
    return pcm




def fig_2c(ax, datapath="data"):
    labels = ['(020)','(0$\overline{2}$0)', '(1$\overline{1}$0)', '(110)', '(2$\overline{3}$0)','(450)', '($\overline{53}$0)']

    dat = np.loadtxt(datapath + '/20200225_meas8_transients_from_fitted_amp_helene.txt')
    time = dat[:,0]
    a = dat[:,1::2]
    error = dat[:,2::2] 
    
    datfit020 = np.loadtxt(datapath + '/fit_0_20.txt')
    tsamp = datfit020[:,0]
    fit020 = datfit020[:,1]
    
    datfit230 = np.loadtxt(datapath + '/fit_230.txt')
    tsamp = datfit230[:,0]
    fit230 = datfit230[:,1]
    
    datfit450 = np.loadtxt(datapath + '/fit_450.txt')
    tsamp = datfit450[:,0]
    fit450 = datfit450[:,1]
    
    datfit530 = np.loadtxt(datapath + '/fit_530.txt')
    tsamp = datfit530[:,0]
    fit530 = datfit530[:,1]
    
    r = Rectangle((0, 0.8), 10, 0.3, color = 'grey', alpha = 0.2)
    ax.add_patch(r)
    
    ax.plot(time, a[:,9] , label  = '020', color = colors_peaks[1],  marker = 'o', markerfacecolor = 'None', linestyle = 'None')
    ax.plot(tsamp, fit020, color = colors_peaks[1] )
    ax.text(250, 1.01, labels[1], color = colors_peaks[1])
    #ax.fill_between(time,a[:,9]  - error[:,9], a[:,9]  + error[:,9], alpha = 0.2, color = colors_peaks[1])
    
    ax.plot(time, a[:,11] , label  = '230', color = colors_peaks[4],  marker = 'd', markerfacecolor = 'None', linestyle = 'None')
    ax.plot(tsamp, fit230 , color = colors_peaks[4] )
    ax.text(250, 0.95, labels[4], color = colors_peaks[4])
    #ax.fill_between(time,a[:,11]  - error[:,11], a[:,11]  + error[:,11], alpha = 0.2, color = colors_peaks[4])
    
    ax.plot(time, a[:,16] , label  = '350', color = colors_peaks[5],  marker = 's', markerfacecolor = 'None', linestyle = 'None')
    ax.plot(tsamp, fit450 , color = colors_peaks[5] )
    ax.text(250, 0.90, labels[5], color = colors_peaks[5])
    #ax.fill_between(time,a[:,16]  - error[:,16], a[:,16]  + error[:,16], alpha = 0.2, color = colors_peaks[5])
    
    idx4 = 21
    ax.plot(time, a[:,idx4] , label  = '350', color = colors_peaks[6], marker = 'p', markerfacecolor = 'None', linestyle = 'None')
    ax.plot(tsamp, fit530 , color = colors_peaks[6] )
    ax.text(250, 0.82, labels[6], color = colors_peaks[6])
    #ax.fill_between(time,a[:,idx4]  - error[:,idx4], a[:,idx4]  + error[:,idx4], alpha = 0.2, color = colors_peaks[6])
    
    ax.plot([-10, 300], [1, 1], linestyle = 'dashed', linewidth = 0.5, color = 'grey')
    ax.set_ylim(0.80, 1.08)
    ax.set_xlim(-10, 300)
    ax.yaxis.set_label_coords(-0.12,0.5)
    ax.xaxis.set_label_coords(0.5,-0.13)
    loc = plticker.MultipleLocator(base=25) # this locator puts ticks at regular intervals
    ax.xaxis.set_minor_locator(loc)
    loc = plticker.MultipleLocator(base=0.1) # this locator puts ticks at regular intervals
    ax.yaxis.set_major_locator(loc)
    loc = plticker.MultipleLocator(base=0.05) # this locator puts ticks at regular intervals
    ax.yaxis.set_minor_locator(loc)
    
    ax.set_xlabel('Time [ps]')
    ax.set_ylabel('Relative Intensity [a.u.]')


def fig_2d(ax, datapath="data"):
    dat = np.loadtxt(datapath + '/20200303_meas3_transients_from_fitted_Helene.txt')
    time = dat[:,0]
    a = dat[:,1:]
    #error = dat[:,2::2] 
    
    idx1 = 9
    ax.plot(time, a[:,idx1] , label  = '020', color = colors_peaks[0],  marker = 'o', markerfacecolor = 'None', zorder = 0, linewidth = 1, markersize = 3)
    ax.text(4.5, 0.972, labels[0], color = colors_peaks[0])
    #ax.fill_between(time,a[:,9]  - error[:,9], a[:,9]  + error[:,9], alpha = 0.2, color = colors[1])
    
    idx2 = 8
    ax.plot(time, a[:,idx2] , label  = '230', color = colors_peaks[1], marker = 'd', markerfacecolor = 'None', zorder = 0, linewidth = 1, markersize = 3)
    ax.text(4.5, 0.964, labels[1], color = colors_peaks[1])
    #ax.fill_between(time,a[:,11]  - error[:,11], a[:,11]  + error[:,11], alpha = 0.2, color = colors[2])
    
    idx3 = 6
    ax.plot(time, a[:,idx3] , label  = '350', color = colors_peaks[2], marker = 's', markerfacecolor = 'None', zorder = 0, linewidth = 1, alpha = 0.5, markersize = 3)
    ax.text(1.7, 1.01, labels[2], color = colors_peaks[2])
    #ax.fill_between(time,a[:,16]  - error[:,16], a[:,16]  + error[:,16], alpha = 0.2, color = colors[5])
    
    idx4 = 7
    ax.plot(time, a[:,idx4] , label  = '350', color = colors_peaks[3], marker = 'p', markerfacecolor = 'None', zorder = 0, linewidth = 1, alpha = 0.5, markersize = 3)
    ax.text(1.7, 1.003, labels[3], color = colors_peaks[3])
    #ax.fill_between(time,a[:,idx4]  - error[:,idx4], a[:,idx4]  + error[:,idx4], alpha = 0.2, color = colors[8])
    
    ax.plot([-10, 300], [1, 1], linestyle = 'dashed', linewidth = 0.5, color = 'grey')
    ax.set_ylim(0.96, 1.02)
    ax.set_xlim(-1.5, 12)
    ax.yaxis.set_label_coords(-0.12,0.5)
    ax.xaxis.set_label_coords(0.5,-0.13)
    
    loc = plticker.MultipleLocator(base=1) # this locator puts ticks at regular intervals
    ax.xaxis.set_minor_locator(loc)
    loc = plticker.MultipleLocator(base=0.01) # this locator puts ticks at regular intervals
    ax.yaxis.set_minor_locator(loc)
    
    ax.set_xlabel('Time [ps]')
    ax.set_ylabel('Relative Intensity [a.u.]')
    
    axins = inset_axes(ax, width="100%", height="100%",
                    bbox_to_anchor=(.70, .37, .25, .5),
                    bbox_transform=ax.transAxes)
    fig_2e(axins)
    r = Rectangle((7, 0.98), 3, 0.03, color = 'white', alpha = 0.8)
    ax.add_patch(r)
    
    
def fig_2e(ax, datapath = "data"):
    dat_fft = np.loadtxt(datapath + '/fft_data.txt')
    freq = dat_fft[:,0]
    fft_020 = dat_fft[:,1]
    fft_011 = dat_fft[:,2]
    ax.plot(freq, fft_020, color = colors_peaks[1])
    ax.fill_between(freq, 0, fft_020, color = colors_peaks[1], alpha = 0.4)
    #ax.plot(freq, fft_011, color = colors_peaks[3])
    #ax.fill_between(freq, 0, fft_011, color = colors_peaks[3], alpha = 0.4)
    #plt.plot(freq, np.abs(np.fft.fft(peak020)))
    ax.set_xlim(0.1,3)
    ax.set_ylim(0.03,0.6)
    ax.xaxis.set_label_coords(0.5,-0.25)
    ax.yaxis.set_label_position("right")
    loc = plticker.MultipleLocator(base=0.25) # this locator puts ticks at regular intervals
    ax.xaxis.set_minor_locator(loc)
    ax.set_yticks([])
    ax.set_ylabel('Abs[FFT]', fontsize = 10)
    ax.set_xlabel('Frequency [THz]', fontsize = 10)


w = 4.5
l = 4


def figure_2():
    
    w = 2*3.37
    l = 4
    fig = plt.figure(constrained_layout=False, figsize=(w, l))
    
    
    gs0 = fig.add_gridspec(
            2,
            1,
            height_ratios=[1, 1],
            bottom=0.1,
            top=0.98,
            left=0.02,
            right=0.98,
            hspace=0.27
        )
        
    #gs00 = gs0[0].subgridspec(1, 1, wspace=0.05, width_ratios=[1])
    gs01 = gs0[0].subgridspec(1, 2, wspace=0.18, hspace = 0.1,  width_ratios=[1,2])
    gs02 = gs0[1].subgridspec(1, 2, wspace=0.18, width_ratios=[1,2])
    
    
    #ax_a7 = fig.add_subplot(gs01[0, 3])
    ax_a1 = fig.add_subplot(gs01[0,0])
    ax_a2 = fig.add_subplot(gs01[0, 1])
    ax_a3 = fig.add_subplot(gs02[0,0])
    ax_a4 = fig.add_subplot(gs02[0,1])
    #ax_a4 = SubplotZero(fig, gs01[0, 0])
    #ax_a4 = fig.add_subplot(gs02[0, 0])
    #fig.add_subplot(ax_a4)
    #ax_a1 = fig.add_subplot(gs00[:])
    #ax_a2 = fig.add_subplot(gs01[0, 0])
    #ax_a1 = fig.add_subplot(gs01[0,1])
    #ax_a3 = fig.add_subplot(gs01[0,1])
    
    
#    fig_1b_2(ax_a1)
    
    #add_label(ax_a1, '(a)', fs=10)
    fig_2a(ax_a1)
    #add_label(ax_a2, '(b)', fs=10)
    fig_2c(ax_a2)
    pcm = fig_2b(ax_a3)
    fig_2d(ax_a4)
    
    lratio = 0.05
    rratio = 0.95
    tratio = 0.95
    bratio = 0.17
    
    plt.subplots_adjust(left=lratio, right=rratio, top=tratio, bottom=bratio)
    
    cbaxes = fig.add_axes([0.06, 0.545, 0.2, 0.02])
    cbar = plt.colorbar(pcm, cax=cbaxes, orientation="horizontal")
    cbar.ax.set_xticklabels(['neg', '0', 'pos']) 
    
    plt.savefig("figure2.pdf", dpi = 200, transparent=True)

def figure_2_presentation():
    
    w = 2*2*3.37
    l = 2*4
    fig = plt.figure(constrained_layout=False, figsize=(w, l))
    
    
    gs0 = fig.add_gridspec(
            2,
            1,
            height_ratios=[1, 1],
            bottom=0.1,
            top=0.98,
            left=0.02,
            right=0.98,
            hspace=0.27
        )
        
    #gs00 = gs0[0].subgridspec(1, 1, wspace=0.05, width_ratios=[1])
    gs01 = gs0[0].subgridspec(1, 2, wspace=0.18, hspace = 0.1,  width_ratios=[1,2])
    gs02 = gs0[1].subgridspec(1, 2, wspace=0.18, width_ratios=[1,2])
    
    
    #ax_a7 = fig.add_subplot(gs01[0, 3])
    ax_a1 = fig.add_subplot(gs01[0,0])
    ax_a2 = fig.add_subplot(gs01[0, 1])
    ax_a3 = fig.add_subplot(gs02[0,0])
    ax_a4 = fig.add_subplot(gs02[0,1])
    #ax_a4 = SubplotZero(fig, gs01[0, 0])
    #ax_a4 = fig.add_subplot(gs02[0, 0])
    #fig.add_subplot(ax_a4)
    #ax_a1 = fig.add_subplot(gs00[:])
    #ax_a2 = fig.add_subplot(gs01[0, 0])
    #ax_a1 = fig.add_subplot(gs01[0,1])
    #ax_a3 = fig.add_subplot(gs01[0,1])
    
    
#    fig_1b_2(ax_a1)
    
    #add_label(ax_a1, '(a)', fs=10)
    fig_2a(ax_a1)
    #add_label(ax_a2, '(b)', fs=10)
    fig_2c(ax_a2)
    pcm = fig_2b(ax_a3)
    fig_2d(ax_a4)
    
    lratio = 0.05
    rratio = 0.95
    tratio = 0.95
    bratio = 0.17
    
    plt.subplots_adjust(left=lratio, right=rratio, top=tratio, bottom=bratio)
    
    cbaxes = fig.add_axes([0.06, 0.545, 0.2, 0.02])
    cbar = plt.colorbar(pcm, cax=cbaxes, orientation="horizontal")
    cbar.ax.set_xticklabels(['neg', '0', 'pos']) 
    
    plt.savefig("figure2_presentation.pdf", dpi = 200, transparent=True)

#w= 6
#l = 4
#fig = plt.figure(constrained_layout=False, figsize=(w, l))
#ax = plt.gca()       
#fig_2d(ax)
#lratio = 0.15
#rratio = 0.95
#tratio = 0.95
#bratio = 0.2
#plt.subplots_adjust(left=lratio, right=rratio, top=tratio, bottom=bratio)
#plt.savefig('Figure3d_presentation.png', dpi = 600)
figure_2()